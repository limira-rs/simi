//! Node for text or comment

/// Just a wrapper arround web_sys::Node
pub struct WebSysNode {
    real_node: web_sys::Node,
}

impl WebSysNode {
    /// Create a text node from a literal
    pub fn new_text(
        text: &str,
        real_parent: &web_sys::Node,
        next_sibling: Option<&web_sys::Node>,
    ) -> Self {
        let real_node: web_sys::Node = crate::interop::document().create_text_node(text).into();
        crate::error::log_result_error(real_parent.insert_before(&real_node, next_sibling));
        Self { real_node }
    }

    /// Create a comment node from a literal
    pub fn new_comment(
        text: &str,
        real_parent: &web_sys::Node,
        next_sibling: Option<&web_sys::Node>,
    ) -> Self {
        let real_node: web_sys::Node = crate::interop::document().create_comment(text).into();
        crate::error::log_result_error(real_parent.insert_before(&real_node, next_sibling));
        Self { real_node }
    }

    // Remove real node from real dom and return the next sibling
    pub(crate) fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &web_sys::Node,
    ) -> Option<web_sys::Node> {
        let next_sibling: Option<web_sys::Node> = self.real_node.next_sibling();
        crate::error::log_result_error(real_parent.remove_child(&self.real_node));
        next_sibling
    }

    // Remove real node from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &web_sys::Node) {
        crate::error::log_result_error(real_parent.remove_child(&self.real_node));
    }

    /// Get next sibling of the last node
    pub fn get_next_sibling(&self) -> Option<web_sys::Node> {
        self.real_node.next_sibling()
    }

    /// Get first real node
    pub fn get_first_real_node(&self) -> Option<&web_sys::Node> {
        Some(&self.real_node)
    }

    /// Clone everything except for tracked attributes, and for-loop content
    pub(super) fn clone(&self, real_parent: &web_sys::Node) -> Self {
        let clone = Self {
            real_node: self
                .real_node
                .clone_node_with_deep(false)
                .expect("clone real_node"),
        };
        crate::error::log_result_error(real_parent.append_child(&clone.real_node));
        clone
    }

    /// Start cloning self and all childs
    pub(super) fn start_clone(&self) -> Self {
        Self {
            real_node: self
                .real_node
                .clone_node_with_deep(false)
                .expect("clone real_node"),
        }
    }
}
