//! Manage all types of node in simi dom
use super::{Arm, Component, Element, KeyedFor, NonKeyedFor, SubApp, Text, WebSysNode};

/// A node in simi dom
pub enum Node {
    /// A text node built directly from a Rust literal or a no-update-expression
    /// Or a comment node
    NeverChange(WebSysNode),
    /// A text node built from an expression
    TextExpression(Text),
    /// An html element
    Element(Element),
    /// For the `for` loop without a key on its items
    NonKeyedFor(NonKeyedFor),
    /// For the `for` loop with a key for each item
    KeyedFor(KeyedFor),
    /// Active arm of `if`/`match`
    Arm(Arm),
    /// A component
    Component(Component),
    /// A sub app
    SubApp(SubApp),
}

impl Node {
    // Remove real node from real dom and return the next sibling
    pub(crate) fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &web_sys::Node,
    ) -> Option<web_sys::Node> {
        match self {
            Node::NeverChange(item) => item.remove_and_get_next_sibling(real_parent),
            Node::TextExpression(item) => item.remove_and_get_next_sibling(real_parent),
            Node::Element(item) => item.remove_and_get_next_sibling(real_parent),
            Node::NonKeyedFor(ref mut item) => item.remove_and_get_next_sibling(real_parent),
            Node::KeyedFor(ref mut item) => item.remove_and_get_next_sibling(real_parent),
            Node::Arm(ref mut item) => item.remove_and_get_next_sibling(real_parent),
            Node::Component(ref mut item) => item.remove_and_get_next_sibling(real_parent),
            Node::SubApp(_) => None,
        }
    }

    // Remove real node from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &web_sys::Node) {
        match self {
            Node::NeverChange(item) => item.remove_real_node(real_parent),
            Node::TextExpression(item) => item.remove_real_node(real_parent),
            Node::Element(item) => item.remove_real_node(real_parent),
            Node::NonKeyedFor(item) => item.remove_real_node(real_parent),
            Node::KeyedFor(item) => item.remove_real_node(real_parent),
            Node::Arm(item) => item.remove_real_node(real_parent),
            Node::Component(item) => item.remove_real_node(real_parent),
            Node::SubApp(item) => item.remove_real_node(real_parent),
        }
    }

    /// Get next sibling of the last node
    pub fn get_next_sibling(&self) -> Option<web_sys::Node> {
        match self {
            Node::NeverChange(item) => item.get_next_sibling(),
            Node::TextExpression(item) => item.get_next_sibling(),
            Node::Element(item) => item.get_next_sibling(),
            Node::NonKeyedFor(item) => item.get_next_sibling(),
            Node::KeyedFor(item) => item.get_next_sibling(),
            Node::Arm(item) => item.get_next_sibling(),
            Node::Component(item) => item.get_next_sibling(),
            Node::SubApp(_) => None,
        }
    }

    /// Get first real node
    pub fn get_first_real_node(&self) -> Option<&web_sys::Node> {
        match self {
            Node::NeverChange(item) => item.get_first_real_node(),
            Node::TextExpression(item) => item.get_first_real_node(),
            Node::Element(item) => item.get_first_real_node(),
            Node::NonKeyedFor(item) => item.get_first_real_node(),
            Node::KeyedFor(item) => item.get_first_real_node(),
            Node::Arm(item) => item.get_first_real_node(),
            Node::Component(item) => item.get_first_real_node(),
            Node::SubApp(_) => None,
        }
    }

    /// Clone everything except for tracked attributes, and for-loop content
    pub(super) fn clone(&self, real_parent: &web_sys::Node) -> Self {
        match self {
            Node::NeverChange(item) => Node::NeverChange(item.clone(real_parent)),
            Node::TextExpression(item) => Node::TextExpression(item.clone(real_parent)),
            Node::Element(item) => Node::Element(item.clone(real_parent)),
            Node::NonKeyedFor(item) => Node::NonKeyedFor(item.clone(real_parent)),
            Node::KeyedFor(item) => Node::KeyedFor(item.clone(real_parent)),
            Node::Arm(item) => Node::Arm(item.clone(real_parent)),
            Node::Component(item) => Node::Component(item.clone(real_parent)),
            Node::SubApp(_) => unreachable!(),
        }
    }

    /// Start cloning self and all childs
    pub(super) fn start_clone(&self) -> Self {
        match self {
            Node::NeverChange(item) => Node::NeverChange(item.start_clone()),
            Node::TextExpression(item) => Node::TextExpression(item.start_clone()),
            Node::Element(item) => Node::Element(item.start_clone()),
            Node::NonKeyedFor(item) => Node::NonKeyedFor(item.start_clone()),
            Node::KeyedFor(item) => Node::KeyedFor(item.start_clone()),
            Node::Arm(item) => Node::Arm(item.start_clone()),
            Node::Component(item) => Node::Component(item.start_clone()),
            Node::SubApp(_) => unreachable!(),
        }
    }
}
