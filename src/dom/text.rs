//! Implement a virtual Text node

/// A node that is rendered as a web_sys::Text which the content is from a user expression that may require update in the future
pub struct Text {
    text: String,
    real_node: web_sys::Node,
}

impl Text {
    /// Create a text node from an expression
    pub fn new(text: String) -> Self {
        let real_node: web_sys::Node = crate::interop::document().create_text_node(&text).into();
        Self { text, real_node }
    }

    /// Update the node if the given `text` is new
    pub fn update_text(&mut self, text: String) {
        if self.text != text {
            self.text = text;
            self.real_node.set_text_content(Some(&self.text));
        }
    }

    /// Insert real_node into the given parent
    pub fn insert_to(&self, real_parent: &web_sys::Node, next_sibling: Option<&web_sys::Node>) {
        crate::error::log_result_error(real_parent.insert_before(&self.real_node, next_sibling));
    }

    // Remove real node from real dom and return the next sibling
    pub(crate) fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &web_sys::Node,
    ) -> Option<web_sys::Node> {
        let next_sibling: Option<web_sys::Node> = self.real_node.next_sibling();
        crate::error::log_result_error(real_parent.remove_child(&self.real_node));
        next_sibling
    }

    // Remove real node from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &web_sys::Node) {
        crate::error::log_result_error(real_parent.remove_child(&self.real_node));
    }

    /// Get next sibling of the last node
    pub fn get_next_sibling(&self) -> Option<web_sys::Node> {
        self.real_node.next_sibling()
    }

    /// Get first real node
    pub fn get_first_real_node(&self) -> Option<&web_sys::Node> {
        Some(&self.real_node)
    }

    /// Clone everything except for tracked attributes, and for-loop content
    pub(super) fn clone(&self, real_parent: &web_sys::Node) -> Self {
        let clone = Self {
            text: String::new(),
            real_node: self
                .real_node
                .clone_node_with_deep(false)
                .expect("clone real_node"),
        };
        crate::error::log_result_error(real_parent.append_child(&clone.real_node));
        clone
    }

    /// Start cloning self and all childs
    pub(super) fn start_clone(&self) -> Self {
        Self {
            text: String::new(),
            real_node: self
                .real_node
                .clone_node_with_deep(false)
                .expect("clone real_node"),
        }
    }
}
