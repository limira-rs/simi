//! A framework for building wasm front-end web application in Rust. Simi is inspired by [Yew](https://github.com/DenisKolodin/yew)
//!
#![deny(missing_docs)]

// Reexport this to make interop::context_callback::context_callback_with_serde_arg! work.
// TODO: How to avoid this?
#[allow(missing_docs)]
#[cfg(feature = "js_callback_with_serde_arg")]
pub use serde_json;
#[cfg(not(feature="nightly"))]
use proc_macro_hack::proc_macro_hack;

pub mod app;
pub mod callback;
pub mod dom;
pub mod events;
pub mod error;
#[cfg(feature = "fetch")]
pub mod fetch;
pub mod interop;

fn set_panic_hook_once() {
    use std::sync::{Once, ONCE_INIT};
    static SET_HOOK: Once = ONCE_INIT;
    SET_HOOK.call_once(|| {
        std::panic::set_hook(Box::new(|panic_info| {
            web_sys::console::error_1(&wasm_bindgen::JsValue::from(panic_info.to_string()));
        }));
    });
}

/// Common use items for a simi app
pub mod prelude {
    pub use crate::app::*;
    pub use crate::events::ElementEventHandler;
    pub use simi_macros::simi_app;
    // This is convenient for users who use "nightly" for both development and deployment
    #[cfg(feature="nightly")]
    pub use crate::{application, component};
}

#[cfg(feature="nightly")]
pub use simi_macros::{application, component};

#[cfg(not(feature="nightly"))]
#[cfg_attr(not(feature="nightly"), proc_macro_hack)]
/// Re-export macros 
pub use simi_macros::{application, component};

/// Hint on how the list used in `for-loop` changed.
/// Currently, ItemValueChangeOnly and MixedChange are process exactly as NoHint
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ListChangeHint {
    /// User does not provide a hint.
    NoHint,
    /// No change occur. Every items are the same as in previous render, just ignore this list when update.
    NoChange,
    /// No item removed or added, but there is some changes in items' content.
    /// Items' order change included in this.
    ItemValueChangeOnly,
    /// New list has no new item added, just has some items removed.
    ItemRemoveOnly,
    /// New list has some new items added, no item removed.
    ItemAddOnly,
    /// Some item removed, some other item added, some change their values.
    MixedChange,
}

/// Alias for Cell<ListChangeHint>
pub type CellHint = std::cell::Cell<ListChangeHint>;
