//! Implement handlers that send message from html elements back to a simi app

use crate::app::{Application, WeakMain};
use wasm_bindgen::closure::Closure;
use wasm_bindgen::JsCast;

/// A trait for managing html element event listeners
pub trait Listener {
    /// `click`, `input`...
    fn event_name(&self) -> &str;
    /// Get js_sys::Function
    fn js_function(&self) -> &js_sys::Function;
}

/// Provides methods to attach handlers to real html elements
pub trait EventHandler<A: Application> {
    /// Create and attach event listener to the given element
    fn create_event_listener(&mut self, event_target: &web_sys::EventTarget, main: &WeakMain<A>) -> Box<Listener>;
}

/// A type use in a component to receive handler passed from the app.
pub type ElementEventHandler<A> = ::std::cell::Cell<Option<Box<EventHandler<A>>>>;

// This may cause some confusion
// $EventType in simi have the same names as its equivalent type in web_sys
// So in this macro, $EventType is used for both simi type and web_sys type
macro_rules! impl_element_events_that_send_message_back_to_simi_app {
    (
        $(($handler_doc:literal, $EventType:ident, $listener_doc:literal, $ListenerType:ident))+
    ) => {$(
        #[doc = $handler_doc]
        pub struct $EventType<F> {
            event_name: &'static str,
            handler: Option<F>,
        }

        impl<F> $EventType<F> {
            /// Create a new event handler from a handler
            pub fn new_raw(event_name: &'static str, handler: F) -> Self {
                Self {
                    event_name,
                    handler: Some(handler),
                }
            }
        }

        impl<F,A> EventHandler<A> for $EventType<F>
        where
            A: Application,
            F: Fn(web_sys::$EventType) -> A::Message + 'static,
        {
            /// Create and attach event listener to the given element
            fn create_event_listener(&mut self, event_target: &web_sys::EventTarget, main: &WeakMain<A>) -> Box<Listener> { 
                let main = main.clone();
                let handler = self.handler.take().expect("Event handler");
                let handler = move |val: web_sys::$EventType| {
                    main.send_message(handler(val));
                };
                let closure = Closure::wrap(Box::new(handler) as Box<Fn(web_sys::$EventType)>);
                Box::new($ListenerType::new(self.event_name, event_target, closure))
            }
        }

        #[doc = $listener_doc]
        pub(crate) struct $ListenerType {
            event_name: &'static str,
            event_target: web_sys::EventTarget,
            handler: Closure<Fn(web_sys::$EventType)>,
        }

        impl $ListenerType {
            /// Create and attach
            fn new(event_name: &'static str, event_target: &web_sys::EventTarget, handler: Closure<Fn(web_sys::$EventType)>) -> Self {
                crate::error::log_result_error(
                    event_target.add_event_listener_with_callback(event_name, handler.as_ref().unchecked_ref()),
                );
                Self {
                    event_name,
                    event_target: event_target.clone(),
                    handler,
                }
            }
        }

        impl Listener for $ListenerType {
            /// Event name of the listener
            fn event_name(&self) -> &str {
                self.event_name
            }
            /// Get js function
            fn js_function(&self) -> &js_sys::Function{
                self.handler.as_ref().unchecked_ref()
            }
        }

        impl Drop for $ListenerType {
            #[inline]
            fn drop(&mut self) {
                crate::error::log_result_error(
                    self.event_target
                        .remove_event_listener_with_callback(self.event_name, self.handler.as_ref().unchecked_ref())
                );
            }
        }
    )+};
}

impl_element_events_that_send_message_back_to_simi_app! {
    (
        "Event handler for FocusEvent",
        FocusEvent,
        "Event listener for FocusEvent",
        FocusEventListener
    )
    (
        "Event handler for MouseEvent",
        MouseEvent,
        "Event listener for MouseEvent",
        MouseEventListener
    )
    (
        "Event handler for WheelEvent",
        WheelEvent,
        "Event listener for WheelEvent",
        WheelEventListener
    )
    (
        "Event handler for UiEvent",
        UiEvent,
        "Event listener for UiEvent",
        UiEventListener
    )
    (
        "Event handler for InputEvent",
        InputEvent,
        "Event listener for InputEvent",
        InputEventListener
    )
    (
        "Event handler for KeyboardEvent",
        KeyboardEvent,
        "Event listener for KeyboardEvent",
        KeyboardEventListener
    )
    (
        "Event handler for Event",
        Event,
        "Event listener for Event",
        EventListener
    )
}


macro_rules! impl_event_helpers {
    ($($EventType:ident { $($EventName:ident => $event_name:literal,)+ })+) => {
        $(
            $(
                #[doc = "Represent "]
                #[doc = $event_name]
                #[doc = " event"]
                pub struct $EventName;
                impl $EventName
                {
                    /// Create an event listener that listen to the specified event on the Window
                    #[doc = "Create and register a simi callback to "]
                    #[doc = $event_name]
                    #[doc = " on the Window"]
                    pub fn listen_on_window<A, F>(&self, main: &WeakMain<A>, handler: F) -> Box<Listener>
                    where
                        A: Application,
                        F: Fn(web_sys::$EventType) -> A::Message  + 'static,
                    {
                        $EventType::new_raw($event_name, handler)
                            .create_event_listener(crate::interop::window().as_ref(), main)
                    }
                }
            )+
        )+
    };
}

// This have partially duplication with macros/render/helper.rs, how to avoid it?
// => Write a procedural macro version of this macro and call it here?
impl_event_helpers! {
    FocusEvent {
        Focus => "focus",
        Blur => "blur",
    }
    MouseEvent {
        AuxClick => "auxclick",
        Click => "click",
        DblClick => "dblclick",
        DoubleClick => "dblclick",
        MouseEnter => "mouseenter",
        MouseOver => "mouseover",
        MouseMove => "mousemove",
        MouseDown => "mousedown",
        MouseUp => "mouseup",
        MouseLeave => "mouseleave",
        MouseOut => "mouseout",
        ContextMenu => "contextmenu",
    }
    WheelEvent {
        Wheel => "wheel",
    }
    UiEvent {
        UiSelect => "select",
    }
    InputEvent {
        Input => "input",
    }
    KeyboardEvent {
        KeyDown => "keydown",
        KeyPress => "keypress",
        KeyUp => "keyup",
    }
    Event {
        Change => "change",
        Reset => "reset",
        Submit => "submit",
        PointerLockChange => "pointerlockchange",
        PointerLockError => "pointerlockerror",
    }
}
