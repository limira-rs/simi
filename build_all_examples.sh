#!/bin/bash

set -e

TARGET="--target=wasm32-unknown-unknown"

echo "Build all ./examples"
for x in ./examples/*; do
    cargo +nightly build $TARGET --manifest-path=$x/Cargo.toml
done

echo "Build all ./stable-examples"
for x in ./stable-examples/*; do
    cargo build $TARGET --manifest-path=$x/Cargo.toml
done
