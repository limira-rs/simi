# Simi [Inactive]


*I turn my attention to [https://gitlab.com/limira-rs/mika] now! It is a signal-based framework!*



A framework for building wasm front-end web application in Rust. Simi is inspired by [Yew](https://github.com/DenisKolodin/yew)

This is still a WIP. Breaking changes will occurs frequently.

* No webpack, no nodejs
* Component, nested component
* Sub-apps
    * Your main app can have multiple sub-apps in it. A sub-app render its own view, independly from the parent app. Parent can send messages to sub-app and vice versa.
* Try to avoid generating new virtual DOM on updating
    * Only generate new virtual DOM for some special cases
* Auto compile `.scss` to `.css`, with the help of [`simi-cli`](https://gitlab.com/limira-rs/simi-cli) (thanks to [rsass](https://github.com/kaj/rsass))

## Documentation

The [Guide](https://gitlab.com/limira-rs/simi/tree/master/guide) provide more infomation on working with Simi. Here is the guide content:

* [Introduction](./guide/introduction.md)
* [A simple example](./guide/simple-example.md)
* [`#[simi_app]`](./guide/simi_app.md)
* [`application!` and `component!`](./guide/macros.md)
* [`Special attributes](./guide/special-attributes.md)
* [Component](./guide/component.md)
* [Sub apps](./guide/sub-apps.md)
* [Simi app's architecture](./guide/architecture.md)
* [Create, build and serve](./guide/run-simi-app.md)
* [Test your simi app](./guide/test-simi-app.md)
* [Contribution](./guide/contribution.md)


## Build on stable Rust
For more information on how to build and serve a simi app, please see the guide (link in the Documentation section above).

Simi v0.2.1 supports build on stable Rust. But only examples in `stable-examples/*` can be successfully built on stable Rust now (`examples/*` still requires Rust nightly).

If you use `simi-cli`, intstall v0.1.8 or from git master and run `simi build -s` or `simi build --stable` in an example folder, for example, in `stable-examples/counter`. Or `simi serve -s` and visit `localhost:8000`.

## Benchmark

A benchmark for Simi is submitted to https://github.com/krausest/js-framework-benchmark. [Here is a snapshot result](https://rawgit.com/krausest/js-framework-benchmark/master/webdriver-ts-results/table.html) (see non-keyed result only, Simi does not support keyed-for-loop yet). Please note that Stdweb and Yew's benchmark binaries are built by Rust nightly-2018-07-11. Simi is built with Rust nightly around 2018-12-7->9 (don’t remember exactly). And a note from the benchmark repo:

> The current snapshot that may not have the same quality (i.e. results might be for mixed browser versions, number of runs per benchmark may vary)

## License

I do not familar with any license before so I am not sure what license to choose. I just draft my own minimal license in `LICENSE` at the root of this repo.