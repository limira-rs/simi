#![feature(proc_macro_hygiene)]
#![cfg(target_arch = "wasm32")]
use simi::prelude::*;
use simi::{CellHint, ListChangeHint};
use wasm_bindgen::prelude::*;
use wasm_bindgen_test::*;

wasm_bindgen_test_configure!(run_in_browser);

#[simi_app]
struct TestApp {
    last_key: u64,
    list: Vec<(u64, String)>,
    hint1: CellHint,
    hint2: CellHint,
}

enum Msg {
    New(String),
    Insert(usize, String),
    Remove(usize),
    Change(usize, String),
}

impl TestApp {
    fn get_next_key(&mut self) -> u64 {
        self.last_key += 1;
        self.last_key
    }
}

impl Application for TestApp {
    type Message = Msg;
    type Context = ();
    type Parent = ();
    fn init() -> Self {
        Self {
            last_key: 2,
            list: vec![(1, "First".to_string()), (2, "Second".to_string())],
            hint1: CellHint::new(ListChangeHint::ItemAddOnly),
            hint2: CellHint::new(ListChangeHint::ItemAddOnly),
        }
    }
    fn update(&mut self, m: Self::Message, _main: ContextPlus<Self>) -> UpdateView {
        match m {
            Msg::New(s) => {
                let key = self.get_next_key();
                self.list.push((key, s));
                self.hint1.set(ListChangeHint::ItemAddOnly);
                self.hint2.set(ListChangeHint::ItemAddOnly);
            }
            Msg::Insert(index, s) => {
                let key = self.get_next_key();
                self.list.insert(index, (key, s));
                self.hint1.set(ListChangeHint::ItemAddOnly);
                self.hint2.set(ListChangeHint::ItemAddOnly);
            }
            Msg::Remove(index) => {
                self.list.remove(index);
                self.hint1.set(ListChangeHint::ItemRemoveOnly);
                self.hint2.set(ListChangeHint::ItemRemoveOnly);
            }
            Msg::Change(index, s) => {
                self.list[index].1 = s;
                self.hint1.set(ListChangeHint::ItemValueChangeOnly);
                self.hint2.set(ListChangeHint::ItemValueChangeOnly);
            }
        }
        true
    }

    fn render(&self, context: RenderContext<Self>) {
        application! {
            //@debug
            div (id="text-item-non-keyed-for") {
                for item in self.list.iter() {
                    @item_count=self.list.len();
                    item.1
                }
            }

            div (id="element-item-non-keyed-for") {
                for item in self.list.iter() {
                    @item_count=self.list.len();
                    s { item.1 }
                }
            }

            div (id="text-item-non-hinted-keyed-for") {
                for item in self.list.iter() {
                    @item_count=self.list.len();
                    @for_item_key = item.0;
                    item.1
                }
            }

            div (id="element-item-non-hinted-keyed-for") {
                for item in self.list.iter() {
                    @item_count=self.list.len();
                    @for_item_key = item.0;
                    em {
                        item.1
                    }
                }
            }

            section (id="text-item-hinted-keyed-for") {
                for item in self.list.iter() {
                    @item_count=self.list.len();
                    @for_hint = self.hint1;
                    @for_item_key = item.0;
                    item.1
                }
            }

            p (id="element-item-hinted-keyed-for") {
                for item in self.list.iter() {
                    @item_count=self.list.len();
                    @for_hint = self.hint2;
                    @for_item_key = item.0;
                    b {
                        item.1
                    }
                }
            }
        }
    }
}

#[wasm_bindgen_test]
fn text_item_non_keyed_for() {
    let main: RcMain<TestApp> = simi_test::start_app();
    let div_text = simi_test::Element::id("text-item-non-keyed-for");
    assert_eq!(
        "FirstSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::New("Third".to_string()));
    assert_eq!(
        "FirstSecondThird",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(2));
    assert_eq!(
        "FirstSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Insert(0, "Forth".to_string()));
    assert_eq!(
        "ForthFirstSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(1));
    assert_eq!(
        "ForthSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!(
        "Second",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!("", div_text.as_node().text_content().expect("div content"));
}

#[wasm_bindgen_test]
fn element_item_non_keyed_for() {
    let main: RcMain<TestApp> = simi_test::start_app();
    let div_element = simi_test::Element::id("element-item-non-keyed-for");
    assert_eq!(
        "FirstSecond",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::New("Third".to_string()));
    assert_eq!(
        "FirstSecondThird",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(2));
    assert_eq!(
        "FirstSecond",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Insert(0, "Forth".to_string()));
    assert_eq!(
        "ForthFirstSecond",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(1));
    assert_eq!(
        "ForthSecond",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!(
        "Second",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!(
        "",
        div_element.as_node().text_content().expect("div content")
    );
}

#[wasm_bindgen_test]
fn text_item_non_hinted_keyed_for() {
    let main: RcMain<TestApp> = simi_test::start_app();
    let div_text = simi_test::Element::id("text-item-non-hinted-keyed-for");
    assert_eq!(
        "FirstSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::New("Third".to_string()));
    assert_eq!(
        "FirstSecondThird",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(2));
    assert_eq!(
        "FirstSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Insert(0, "Forth".to_string()));
    assert_eq!(
        "ForthFirstSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(1));
    assert_eq!(
        "ForthSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!(
        "Second",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!("", div_text.as_node().text_content().expect("div content"));
}

#[wasm_bindgen_test]
fn element_item_non_hinted_keyed_for() {
    let main: RcMain<TestApp> = simi_test::start_app();
    let div_element = simi_test::Element::id("element-item-non-hinted-keyed-for");
    assert_eq!(
        "FirstSecond",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::New("Third".to_string()));
    assert_eq!(
        "FirstSecondThird",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(2));
    assert_eq!(
        "FirstSecond",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Insert(0, "Forth".to_string()));
    assert_eq!(
        "ForthFirstSecond",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(1));
    assert_eq!(
        "ForthSecond",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!(
        "Second",
        div_element.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!(
        "",
        div_element.as_node().text_content().expect("div content")
    );
}

#[wasm_bindgen_test]
fn text_item_hinted_keyed_for() {
    let main: RcMain<TestApp> = simi_test::start_app();
    let div_text = simi_test::Element::id("text-item-hinted-keyed-for");
    assert_eq!(
        "FirstSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::New("Third".to_string()));
    assert_eq!(
        "FirstSecondThird",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Change(1, "Middle".to_string()));
    assert_eq!(
        "FirstMiddleThird",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(2));
    main.send_message(Msg::Change(1, "Second".to_string()));
    assert_eq!(
        "FirstSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Insert(0, "Forth".to_string()));
    assert_eq!(
        "ForthFirstSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(1));
    assert_eq!(
        "ForthSecond",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!(
        "Second",
        div_text.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!("", div_text.as_node().text_content().expect("div content"));
}

#[wasm_bindgen_test]
fn element_item_hinted_keyed_for() {
    let main: RcMain<TestApp> = simi_test::start_app();
    let div = simi_test::Element::id("element-item-hinted-keyed-for");
    assert_eq!(
        "FirstSecond",
        div.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::New("Third".to_string()));
    assert_eq!(
        "FirstSecondThird",
        div.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Change(1, "Middle".to_string()));
    assert_eq!(
        "FirstMiddleThird",
        div.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(2));
    main.send_message(Msg::Change(1, "Second".to_string()));
    assert_eq!(
        "FirstSecond",
        div.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Insert(0, "Forth".to_string()));
    assert_eq!(
        "ForthFirstSecond",
        div.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(1));
    assert_eq!(
        "ForthSecond",
        div.as_node().text_content().expect("div content")
    );

    main.send_message(Msg::Remove(0));
    assert_eq!("Second", div.as_node().text_content().expect("div content"));

    main.send_message(Msg::Remove(0));
    assert_eq!("", div.as_node().text_content().expect("div content"));
}
