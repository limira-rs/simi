use simi::prelude::*;

pub struct MessagePanel<'a> {
    pub message: &'a String,
}
impl<A: Application> Component<A> for MessagePanel<'_> {
    fn render(&self, context: RenderContext<A>) {
        component! {
            "Message: " self.message
        }
    }
}

pub struct ValueDisplay {
    pub value: i32,
}

impl<A: Application> Component<A> for ValueDisplay {
    fn render(&self, context: RenderContext<A>) {
        component! {
            "Value: " self.value
        }
    }
}

pub struct Counter<'a, A: Application> {
    pub id: &'static str,
    pub title: &'static str,
    pub value: &'a i32,
    pub up: ElementEventHandler<A>,
    pub down: ElementEventHandler<A>,
}

impl<A: Application> Component<A> for Counter<'_, A> {
    fn render(&self, context: RenderContext<A>) {
        component! {
            div (id=#self.id) {
                p { #self.title self.value }
                button (class="down" onclick=#self.down) { "Down" }
                button (class="up" onclick=#self.up) { "Up" }
            }
        }
    }
}

pub struct ParentComponent<'a, A: Application> {
    pub child1: Box<Component<A> + 'a>,
    pub child2: Box<Component<A> + 'a>,
}

impl<A: Application> Component<A> for ParentComponent<'_, A> {
    fn render(&self, context: RenderContext<A>) {
        component! {
            $ self.child1
            $ self.child2
        }
    }
}
