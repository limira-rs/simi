# A simple example

The following example is copied-pasted from `examples/counter/lib.rs`, with some comments added.

```rust
// Unfornately, simi-macro require this feature to work, therefore, Simi only works with Rust-nightly.
#![feature(proc_macro_hygiene)]

use simi::prelude::*;
use wasm_bindgen::prelude::*;

// #[simi_app] is a `proc_macro_attribute`, it use `wasm-bindgen` internally to generate glue code for simi app to be created from JavaScript.
#[simi_app]
// Counter is the app state.
struct Counter {
    count: i32,
}

enum Msg {
    Up,
    Down,
}

// Implement the simi app trait for the app state.
impl Application for Counter {
    type Message = Msg;
    type Context = ();
    type Parent = ();
    fn init() -> Self {
        Self { count: 2018 }
    }
    fn update(&mut self, m: Self::Message, _: ContextPlus<Self>) -> UpdateView {
        match m {
            Msg::Up => self.count += 1,
            Msg::Down => self.count -= 1,
        }
        true
    }
    fn render(&self, context: RenderContext<Self>) {
        //           ^^^^^^^ `application!` access this argument. (The name must always be `context`).

        // This macro generates code for creating and updating the app's view
        application!{
            // If you want to see the generated code, uncomment the next line the build your code again.
            // @debug

            // Because the content of <h1> and <p> never changes, the are *almost* ignored when updating.
            h1 { "Counter" }
            p { "the " b {"simplest"} " version" }

            // <hr/>
            hr

            // The `#` before Msg::Down indicates that the message send by this button will never change.
            // Hence, the whole button has nothing to update. It is also *almost* ignored when updating.
            button (onclick=#Msg::Down) { "Down" }

            // The content of <span> has an expression `self.count` that may change, it will be checked
            // every time the `render` method is executed.
            span { " " self.count " " }

            button (onclick=#Msg::Up) { "Up" }
        }
    }
}
```
