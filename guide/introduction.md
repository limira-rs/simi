# Introduction

Simi is a framework for building frontend webapp in Rust. Simi is inspired by [Yew](https://github.com/DenisKolodin/yew), but the later is based on `stdweb`, while the former builds on top of [`wasm-bindgen`](https://github.com/rustwasm/wasm-bindgen).

Similar to Yew, Simi also maintains a virtual DOM internally. But the update process is done differently. Yew creates a new virtual DOM on every update, compare the new DOM to the old virtual DOM, do patching if there are differences. Simi only creates a virtual DOM if it is new, if the required DOM already exists, it tries to update the current DOM.

The implementation is very easy for static nodes and nodes created by `if` or `match` arms. All these items can be considered as `keyed` nodes (if I understand `keyed` correctly). Static nodes will never be removed, they only changes their content. Nodes created by `if` or `match` will be updated if they are in the same arm as previous render. If the new update cause `if`/`match` to render an arm differ to the previous render, all previously-rendered-nodes will be removed, and new nodes will be created by the new arm.

## The `for` loop
The non-keyed `for` loop is also easy. Because it does not need to retain the association between the list's element and the real DOM node. Things become complicated for nodes created by the keyed `for` loop. As far as I understand, users might expect a strong link between the keyed-data and view for situation where they use CSS transition. Because the implementation of keyed-`for`-loop is not easy, and because I, the first author of Simi - and maybe (currently) the only user of the framework, do not this feature. So I left this out for now.

tldr: **keyed `for` loop** is not implemented yet.

## Warnings

Despite it is now v0.2, Simi is still in its early stage. Things are at high chance to be changed.

Simi contains some `unsafe` block to get items from `Vec`s in cases where the indices are certain to be in bound. But it is better for you to check all of them if you consider using Simi.

Because of the choosen approach (Simi tries to update the existing virtual DOM, instead of create new virtual DOM and do diffing), Simi may not be considered a true framework. Most of the logic for the *framework* is not implemented by the `simi` crate, but they (the logic) are implemented in the output constructed by the `simi-macros` crate. This approach make Simi hard to understand and more difficult to maintain compare to other frameworks. 

> **TLDR: I am not sure about the future of Simi :D**, specially if there is no clear advantage over other frameworks.

> Only use Simi if you are brave or... crazy enough.

## Alternatives

If you don't like Simi, there are some other choices for frontend wasm web app (alphabetical order):
* dominator: https://github.com/Pauan/rust-dominator
    * On `stdweb`
    * No virtual DOM
    * Use https://github.com/Pauan/rust-signals
* draco: https://github.com/utkarshkukreti/draco
    * No macro
* percy: https://github.com/chinedufn/percy
    * With server side rendering
* ruukh: https://github.com/csharad/ruukh (archived, *discontinued*)
* seed: https://github.com/David-OConnor/seed (one of the most active)
* smithy: https://github.com/rbalicki2/smithy
* squark: https://github.com/rail44/squark
* willow: https://github.com/sindreij/willow (may be just an experiment, the author may have no intention to publish it)
* yew: https://github.com/DenisKolodin/yew
    * The most mature one, it comes before `wasm-bindgen`.
    * On `stdweb`

## Virtual DOM libraries

Theses library are not full frameworks, their aim are for you to build a framework on top of them.
* dodrio: https://github.com/fitzgen/dodrio