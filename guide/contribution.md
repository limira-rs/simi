# Inspect the generated code

You can add `@debug` to the beginning of the macro content like:
```rust
fn render(&self, context: RenderContext<Self>) {
    application! {
        @debug // To output the generated code
        div (id="some-id") {
            ....
        }
    }
}
```

Then, the generated code will be outputed to your terminal. If there is something wrong with the generated code, you can copy it and place it in the `render` method:

```rust
fn render(&self, context: RenderContext<Self>) {
    // Paste the generated code here
    // (instead of the original macro)
}
```
(It's better to duplicate your `.rs` file to keep a file with the original macro)

Save the file and compile your project again to see what's wrong with the generated code!

*However, it is not a good process to debug. Do you have a better idea?*

# Test your changes

You can add your tests to `tests/*`. To run tests, at the root of the repo, enter: `./ci_test_local.sh`. Make sure all tests pass and there is no warnings before pushing.