# Create, build, serve a simi app

## Create new simi app

You can create a new Rust project by run:
```
cargo new your-project-name
```
Then, add this to your `Cargo.toml`

```toml
[lib]
crate-type = ["cdylib"]

[dependencies]
wasm-bindgen = "0.2.29"
simi = "0.2.0"
```

## Serve a simi app

This section shows you how to run an example in simi repo.

You must [install Rust](https://www.rust-lang.org/en-US/) to use `Simi`.

Then:
```
# Simi requires nightly
rustup install nightly

# Because we build for the web
rustup target add wasm32-unknown-unknown --toolchain=nightly

# `Simi` build on top of `wasm-bindgen`
cargo install wasm-bindgen-cli

# Install simi-cli from crates.io
# If you want to install simi-cli from source, see: https://gitlab.com/limira-rs/simi-cli
cargo install simi-cli

# Clone simi source code and try its examples
git clone https://gitlab.com/limira-rs/simi.git
cd examples/counter
simi serve
```

Wait for the build complete, then open your browser and visit http://localhost:8000/!

## Build a simi app
The `simi serve` command in the above section will execute the build process internally.
By default, the build process will output everything to `$crate_root/simi-site`. You can provide 
a custom location via `.simi.toml`. You can find more info at https://gitlab.com/limira-rs/simi-cli

## wasm-bindgen version mismatched

If you encountered error like:
```
error: executing `wasm-bindgen` over the wasm file
    caused by: failed to extract wasm-bindgen custom sections

# ... some lines ommitted ...

  rust wasm file: 0.2.xx
     this binary: 0.2.yy

# ... more lines ommitted ....
```

You may need to run 
```
cargo update -p wasm-bindgen
cargo install -f wasm-bindgen-cli
```

If you still encounter a similar error message, you may need to delete `Cargo.lock` file (it is `.lock`).