This folder contains guide for building a simi app.

* [Introduction](introduction.md)
* [A simple example](simple-example.md)
* [`#[simi_app]`](simi_app.md)
* [`application!` and `component!`](macros.md)
* [`Special attributes](special-attributes.md)
* [Component](component.md)
* [Sub apps](sub-apps.md)
* [Simi app's architecture](architecture.md)
* [Create, build and serve](run-simi-app.md)
* [Test your simi app](test-simi-app.md)
* [Contribution](contribution.md)
