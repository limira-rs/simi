use super::*;
use quote::quote;

fn no_update_item() -> proc_macro2::Punct {
    proc_macro2::Punct::new('#', proc_macro2::Spacing::Alone)
}

impl NodeList {
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        self.nodes
            .iter()
            .for_each(|n| n.collect_no_update_states(states))
    }
}

impl Node {
    fn get_element(&self) -> &Element {
        match self {
            Node::Element(e) => e,
            _ => panic!("Not an element"),
        }
    }
    fn get_expression(&self) -> &SimpleExpression {
        match self {
            Node::Expression(e) => e,
            _ => panic!("Not an expression"),
        }
    }
    fn get_lit_str(&self) -> &LitStr {
        match self {
            Node::Literal(l) => l,
            _ => panic!("Not a literal"),
        }
    }

    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        match self {
            Node::Element(item) => item.collect_no_update_states(states),
            Node::Expression(item) => item.collect_no_update_states(states),
            Node::Literal(_) => states.push(("literal".to_string(), true)),
            // Node::ChildPlaceholder(_) => states.push(("child_placeholder".to_string(), false)),
            Node::For(item) => item.collect_no_update_states(states),
            Node::If(item) => item.collect_no_update_states(states),
            Node::Match(item) => item.collect_no_update_states(states),
            Node::Component(item) => states.push(("compopnent".to_string(), item.no_update)),
            Node::ComponentPlaceholder(_) => states.push(("$".to_string(), false)),
        }
    }
}

impl Element {
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        if let Some(content) = &self.content {
            content.collect_no_update_states(states);
        }
        states.push((self.tag.to_string(), self.no_update));
    }
}

impl SimpleExpression {
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        states.push(("expression".to_string(), self.no_update))
    }
}

impl ElementContent {
    fn get_node_list(&self) -> &NodeList {
        match self {
            ElementContent::NodeList(n) => n,
            _ => panic!("Not a node list"),
        }
    }

    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        if let ElementContent::NodeList(item) = self {
            item.collect_no_update_states(states);
        }
    }
}

impl For {
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        match &self.content {
            ForContent::Element(e) => e.collect_no_update_states(states),
            ForContent::Expression(e) => e.collect_no_update_states(states),
        }
        states.push(("For".to_string(), self.no_update));
    }
}

impl If {
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        self.arms
            .iter()
            .for_each(|a| a.body.node_list.collect_no_update_states(states));
        states.push(("If".to_string(), self.no_update));
    }
}

impl Match {
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        self.arms
            .iter()
            .for_each(|a| a.body.node_list.collect_no_update_states(states));
        states.push(("Match".to_string(), self.no_update));
    }
}

#[test]
fn macro_options() {
    let input = quote! (
        @debug
        div ()
    );

    let rs: MacroInput = syn::parse2(input).expect("macro_options.debug = true");
    assert_eq!(true, rs.options.debug);

    let input = quote!(div());

    let rs: MacroInput = syn::parse2(input).expect("macro_options.debug = false");
    assert_eq!(false, rs.options.debug);
}

#[test]
fn one_empty_element() {
    let input = quote! {
        div()
    };
    let rs: MacroInput = syn::parse2(input).expect("one_empty_element");
    let nodes = rs.node_list.nodes;

    assert_eq!(1, nodes.len());

    match &nodes[0] {
        Node::Element(e) => {
            assert_eq!(false, e.no_update);
            assert_eq!("div", e.tag.to_string());
            assert!(e.attributes.is_none());
        }
        _ => panic!("It must be a Node::Element"),
    }
}

#[test]
fn one_element_with_attributes() {
    let nu = no_update_item();
    let input = quote! {
        input (
            id="some-id"
            class="class1 class-2"
            "no-change-class"=#nu no_update_class
            "may-change-class"=some_bool_value
            value=self.get_some_value()
            onclick=#nu Msg::Click
            onchange=Msg::Change(?)
            oninput=Msg::Input(?)
            data-user-name="Limira"
            data-user-skill="0"
            data-user-strength=some_value
            // More
        )
    };
    let rs: MacroInput = syn::parse2(input).expect("one_element_with_attributes");
    let nodes = rs.node_list.nodes;

    assert_eq!(1, nodes.len());
    match &nodes[0] {
        Node::Element(e) => {
            assert_eq!(false, e.no_update);
            assert_eq!("input", e.tag.to_string());
            match &e.attributes {
                Some(a) => {
                    assert_eq!(3, a.no_update_attributes.len());
                    assert_eq!(2, a.attributes.len());
                    assert_eq!(1, a.literal_classes.len());
                    assert_eq!(1, a.classes.len());
                    assert_eq!(1, a.no_update_classes.len());
                    assert_eq!(2, a.events.len());
                    assert_eq!(1, a.no_update_events.len());
                }
                None => panic!("There must be some attributes"),
            }
        }
        _ => panic!("It must be a Node::Element"),
    }
}

#[test]
fn multi_elements_at_root() {
    let nu = no_update_item();
    let input = quote! {
        #nu div ()
        p ()
        "Some literal"
        #nu span ()
        self.some_value
        a_multi_root_expression
        #nu another_multi_root_expression
        #nu self.multi_root_method_call()
        "More literal"
        "Another literal"
        section ()
    };
    let rs: MacroInput = syn::parse2(input).expect("multi_elements_at_root");
    let nodes = rs.node_list.nodes;
    assert_eq!(11, nodes.len());

    let e = nodes[0].get_element();
    assert_eq!(true, e.no_update);
    assert_eq!("div", e.tag.to_string());

    let e = nodes[1].get_element();
    assert_eq!(false, e.no_update);
    assert_eq!("p", e.tag.to_string());

    let l = nodes[2].get_lit_str();
    assert_eq!("Some literal", l.value());

    let e = nodes[3].get_element();
    assert_eq!(true, e.no_update);
    assert_eq!("span", e.tag.to_string());

    let e = nodes[4].get_expression();
    assert_eq!(false, e.no_update);
    assert_eq!("self . some_value", e.value.to_string());

    let e = nodes[5].get_expression();
    assert_eq!(false, e.no_update);
    assert_eq!("a_multi_root_expression", e.value.to_string());

    let e = nodes[6].get_expression();
    assert_eq!(true, e.no_update);
    assert_eq!("another_multi_root_expression", e.value.to_string());

    let e = nodes[7].get_expression();
    assert_eq!(true, e.no_update);
    assert_eq!("self . multi_root_method_call ( )", e.value.to_string());

    let l = nodes[8].get_lit_str();
    assert_eq!("More literal", l.value());

    let l = nodes[9].get_lit_str();
    assert_eq!("Another literal", l.value());

    let e = nodes[10].get_element();
    assert_eq!(false, e.no_update);
    assert_eq!("section", e.tag.to_string());
}

#[test]
fn nested_elements() {
    let nu = no_update_item();
    let input = quote! {
        p {
            span {
                "Some literal"
                #nu no_update_expression
                "Some other literal"
                may_update_expression
            }
            self.some_method_call()
            input (value=self.some_value)
        }
    };
    let rs: MacroInput = syn::parse2(input).expect("multi_elements_at_root");
    let nodes = rs.node_list.nodes;

    assert_eq!(1, nodes.len());

    let e = nodes[0].get_element();
    assert_eq!("p", e.tag.to_string());

    let nodes = &e
        .content
        .as_ref()
        .expect("Must have a content")
        .get_node_list()
        .nodes;
    assert_eq!(3, nodes.len());

    let e = nodes[0].get_element();
    assert_eq!("span", e.tag.to_string());

    let e = nodes[1].get_expression();
    assert_eq!(false, e.no_update);
    assert_eq!("self . some_method_call ( )", e.value.to_string());

    let e = nodes[2].get_element();
    assert_eq!("input", e.tag.to_string());

    let nodes = &nodes[0]
        .get_element()
        .content
        .as_ref()
        .expect("Must have a content")
        .get_node_list()
        .nodes;
    assert_eq!(4, nodes.len());

    let e = nodes[0].get_lit_str();
    assert_eq!("Some literal", e.value());

    let e = nodes[1].get_expression();
    assert_eq!(true, e.no_update);
    assert_eq!("no_update_expression", e.value.to_string());

    let e = nodes[2].get_lit_str();
    assert_eq!("Some other literal", e.value());

    let e = nodes[3].get_expression();
    assert_eq!(false, e.no_update);
    assert_eq!("may_update_expression", e.value.to_string());
}

#[test]
fn no_update() {
    let no_update = no_update_item();
    let input = quote! {
        section (class="todoapp") {
            header(class="header") {
                h1 { "Simi todos" }
                input (
                    class="new-todo"
                    placeholder="What needs to be done?"
                    autofocus=true
                    onchange=#no_update Msg::New(?)
                )
            }
            section ( class="main" "hidden"=empty_list ) {
                input (
                    id="toggle-all"
                    class="toggle-all"
                    type="checkbox"
                    checked=all_completed
                    onchange=Msg::ToggleAll(!all_completed)
                )
                label ( for="toggle-all" ) { "Mark all as complete" }
                ul ( class="todo-list" ) {
                    for (index,task) in self.data.list().iter().enumerate() {
                        @item_count=some_value;
                        li ( "completed"=task.completed "editing"=self.is_editing(index) ) {
                            div(class="view"){
                                input(
                                    class="toggle"
                                    type="checkbox"
                                    checked=task.completed
                                    onchange=Msg::Toggle(index)
                                )
                                label (ondoubleclick=Msg::StartEditing(index)) { task.title }
                                button ( class="destroy" onclick=Msg::Remove(index) )
                            }
                            input (
                                class="edit"
                                value=task.title
                                onblur=Msg::BlurEditing(?)
                                onkeypress=Msg::EndEditing(?)
                            )
                        }
                    }
                }
            }
            footer ( class="footer" "hidden"=empty_list) {
                span ( class="todo-count" ) {
                    strong {item_left} self.item_left_label(item_left)
                }
                ul ( class="filters" ) {
                    li { a ( class="selected" href="#/" ) {"All"} }
                    li { a ( href="#/active" ) {"Active"} }
                    li { a ( href="#/completed" ) {"Completed"} }
                }
                button (
                    class="clear-completed"
                    "hidden"=completed_count==0
                    onclick=#no_update Msg::ClearCompleted
                ) { "Clear completed" }
            }
        }
        footer ( class="info" ) {
            p { "Double-click to edit a todo" }
            p { "Created by Limira" }
            p { "Part of Simi Project" }
            p { "Part of " a (href="http://todomvc.com") { "TodoMVC" } }
        }
    };
    let rs: MacroInput = syn::parse2(input).expect("no_update");
    let mut node_list = rs.node_list;
    node_list.inspect_no_update();
    let mut states = Vec::new();
    node_list.collect_no_update_states(&mut states);
    let expects = vec![
        //========
        ("literal".to_string(), true),
        ("h1".to_string(), true),
        ("input".to_string(), true),
        ("header".to_string(), true),
        //========
        ("input".to_string(), false),
        ("literal".to_string(), true),
        ("label".to_string(), true),
        //>>>>>>>>
        ("input".to_string(), false),
        ("expression".to_string(), false),
        ("label".to_string(), false),
        ("button".to_string(), false),
        ("div".to_string(), false),
        ("input".to_string(), false),
        ("li".to_string(), false),
        ("For".to_string(), false),
        ("ul".to_string(), false),
        ("section".to_string(), false),
        //========
        ("expression".to_string(), false),
        ("strong".to_string(), false),
        ("expression".to_string(), false),
        ("span".to_string(), false),
        ("literal".to_string(), true),
        ("a".to_string(), true),
        ("li".to_string(), true),
        ("literal".to_string(), true),
        ("a".to_string(), true),
        ("li".to_string(), true),
        ("literal".to_string(), true),
        ("a".to_string(), true),
        ("li".to_string(), true),
        ("ul".to_string(), true),
        ("literal".to_string(), true),
        ("button".to_string(), false),
        ("footer".to_string(), false),
        ("section".to_string(), false),
        //========
        ("literal".to_string(), true),
        ("p".to_string(), true),
        ("literal".to_string(), true),
        ("p".to_string(), true),
        ("literal".to_string(), true),
        ("p".to_string(), true),
        ("literal".to_string(), true),
        ("literal".to_string(), true),
        ("a".to_string(), true),
        ("p".to_string(), true),
        ("footer".to_string(), true),
    ];

    //println!("{:#?}\n{:#?}", expects, states);

    assert_eq!(expects, states);
}
