use super::*;
use quote::quote;

impl NodeList {
    pub fn generate(
        &self,
        params: &Params,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
        simi_element_node_is_new: &TokenStream,
    ) -> TokenStream {
        self.nodes
            .iter()
            .enumerate()
            .map(|(index, node)| {
                node.generate(
                    params,
                    index,
                    node_list,
                    real_parent,
                    next_sibling,
                    simi_element_node_is_new,
                )
            })
            .collect()
    }
}

impl Node {
    fn generate(
        &self,
        params: &Params,
        index: usize,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
        simi_element_node_is_new: &TokenStream,
    ) -> TokenStream {
        match self {
            Node::Literal(item) => {
                quote! {
                    if #simi_element_node_is_new{
                        #node_list.no_update_literal_string(#item, #real_parent, #next_sibling);
                    }
                }
            }
            Node::Expression(item) => item.generate(
                &quote! { #index },
                node_list,
                real_parent,
                next_sibling,
                simi_element_node_is_new,
            ),
            Node::Element(item) => item.generate(
                params,
                &quote! { #index },
                node_list,
                real_parent,
                next_sibling,
                simi_element_node_is_new,
            ),
            Node::For(item) => item.generate(
                params,
                index,
                node_list,
                real_parent,
                simi_element_node_is_new,
            ),
            Node::If(item) => item.generate(
                params,
                index,
                node_list,
                real_parent,
                next_sibling,
                simi_element_node_is_new,
            ),
            Node::Match(item) => item.generate(
                params,
                index,
                node_list,
                real_parent,
                next_sibling,
                simi_element_node_is_new,
            ),
            Node::Component(item) => {
                item.generate(params, index, node_list, real_parent, next_sibling)
            }
            Node::ComponentPlaceholder(child_field) => {
                let child_component_node = params.get_item_name("child_component_node");
                let component_context = params.get_item_name("component_context");
                let context = &params.context;
                quote! {{
                    let (_, #child_component_node) = #node_list.component(#index);
                    let #component_context = RenderContext::new(
                        #child_component_node.node_list_mut(),
                        #real_parent,
                        #context.main.clone(),
                        #next_sibling
                    );
                    //let qwerty = #child_field.take().unwrap();
                    //qwerty.render(#component_context);
                    #child_field.render(#component_context);
                }}
            }
        }
    }
}

impl SimpleExpression {
    fn generate(
        &self,
        index: &TokenStream,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
        simi_element_node_is_new: &TokenStream,
    ) -> TokenStream {
        let value = &self.value;
        if self.no_update {
            quote! {
                if #simi_element_node_is_new {
                    #node_list.no_update_text(&#value, #real_parent, #next_sibling);
                }
            }
        } else {
            quote! {
                if let Some(text) = #node_list.text(#index, &#value) {
                    text.insert_to(#real_parent, #next_sibling);
                }
            }
        }
    }
}

impl Element {
    fn generate(
        &self,
        params: &Params,
        index: &TokenStream,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
        parent_simi_element_node_is_new: &TokenStream,
    ) -> TokenStream {
        let tag = self.tag.to_string();
        let simi_element_node = params.get_item_name("simi_element_node");
        let simi_element_node_is_new = params.get_item_name("simi_element_node_is_new");

        let init_or_update_element = self.generate_init_or_update(
            params,
            &simi_element_node,
            real_parent,
            next_sibling,
            &simi_element_node_is_new,
            &simi_element_node_is_new,
        );
        let create_or_update_element = quote! {{
            let (#simi_element_node, #simi_element_node_is_new) = #node_list.element(#index, #tag);
            #init_or_update_element
        }};
        if self.no_update {
            quote! {
                if #parent_simi_element_node_is_new {
                    #create_or_update_element
                }
            }
        } else {
            create_or_update_element
        }
    }

    fn generate_init_or_update(
        &self,
        params: &Params,
        simi_element_node: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
        need_to_attach_no_update_attributes: &TokenStream,
        need_to_insert_to_real_parent: &TokenStream,
    ) -> TokenStream {
        let tag = self.tag.to_string();
        let new_node_list = params.get_item_name("new_node_list");
        let new_real_element = params.get_item_name("new_real_element");
        let new_next_sibling = quote! { None };
        let (set_no_update_attributes, set_or_update_attributes, tracked_attribute_count) =
            if let Some(a) = &self.attributes {
                let no_update_attributes = a.generate_no_update(params, &tag, &simi_element_node);
                let attributes = a.generate(params, &tag, &simi_element_node);
                (
                    no_update_attributes,
                    attributes,
                    a.attributes.len()
                        + a.classes.len()
                        + a.events.len()
                        // Despite the event listeners do not need to be tracked, but we need to store
                        // them to prevent they from dropping
                        + a.no_update_events.len(),
                )
            } else {
                (TokenStream::new(), TokenStream::new(), 0)
            };
        let (add_or_update_child_nodes, child_node_count) = if let Some(c) = &self.content {
            let childs = c.generate(
                params,
                &new_node_list,
                &new_real_element,
                &new_next_sibling,
                need_to_attach_no_update_attributes,
            );
            (childs, c.child_node_count())
        } else {
            (TokenStream::new(), 0)
        };
        quote! {
            if #need_to_attach_no_update_attributes {
                #simi_element_node.reserve(#tracked_attribute_count, #child_node_count);
                #set_no_update_attributes
            }
            {
                let (#new_node_list, #new_real_element) = #simi_element_node.node_list_mut_and_real_node();
                #add_or_update_child_nodes
            }
            #set_or_update_attributes
            if #need_to_insert_to_real_parent {
                #simi_element_node.insert_to(#real_parent, #next_sibling);
            }

            //simi::interop::console::log(&format!("{}: {}=={}", #tag, #child_node_count, #simi_element_node.node_count()));
            debug_assert_eq!(#tracked_attribute_count, #simi_element_node.attribute_count(), "Attribute count mismatch");
            debug_assert_eq!(#child_node_count, #simi_element_node.node_count(), "Child count mismatch");

        }
    }
}

impl ElementAttributes {
    fn generate_no_update(
        &self,
        params: &Params,
        tag: &str,
        simi_element: &TokenStream,
    ) -> TokenStream {
        let no_update_attributes: TokenStream = self
            .no_update_attributes
            .iter()
            .map(|item| item.generate_no_update(tag, simi_element))
            .collect();
        let literal_classes = if self.literal_classes.is_empty() {
            TokenStream::new()
        } else {
            let literal_classes = self
                .literal_classes
                .iter()
                //.map(|item| item.value())
                .map(syn::LitStr::value)
                .collect::<Vec<String>>()
                .join(" ");
            quote! {
                #simi_element.no_update_literal_string_attribute("class", #literal_classes);
            }
        };
        let no_update_classes: TokenStream = self
            .no_update_classes
            .iter()
            .map(|item| item.generate_no_update(simi_element))
            .collect();
        let no_update_events = self.generate_no_update_events(params, simi_element);
        quote! {
            #no_update_attributes
            #literal_classes
            #no_update_classes
            #no_update_events
        }
    }

    fn generate_no_update_events(
        &self,
        params: &Params,
        simi_element: &TokenStream,
    ) -> TokenStream {
        self.no_update_events
            .iter()
            .enumerate()
            .map(|(index, item)| item.generate(params, index, simi_element))
            .collect()
    }

    fn generate(&self, params: &Params, tag: &str, simi_element: &TokenStream) -> TokenStream {
        let start_index = self.no_update_events.len();
        let events: TokenStream = self
            .events
            .iter()
            .enumerate()
            .map(|(index, item)| item.generate(params, index, simi_element))
            .collect();

        let start_index = start_index + self.events.len();
        let attributes: TokenStream = self
            .attributes
            .iter()
            .enumerate()
            .map(|(index, item)| item.generate(index + start_index, tag, simi_element))
            .collect();

        let start_index = start_index + self.attributes.len();
        let classes: TokenStream = self
            .classes
            .iter()
            .enumerate()
            .map(|(index, item)| item.generate(index + start_index, simi_element))
            .collect();

        quote! {
            #events
            #attributes
            #classes
        }
    }
}

impl ElementAttribute {
    fn generate_no_update(&self, tag: &str, simi_element: &TokenStream) -> TokenStream {
        match self.attribute_type {
            ElementAttributeType::Unknown => {
                panic!("simi-macro bug: self.attribute_type must be known before code generating")
            }
            ElementAttributeType::Bool => self.generate_no_update_bool(tag, simi_element),
            ElementAttributeType::String => self.generate_no_update_string(tag, simi_element),
            _ => unimplemented!("ElementAttributeType::***"),
        }
    }

    fn generate_no_update_bool(&self, _tag: &str, simi_element: &TokenStream) -> TokenStream {
        debug_assert!(self.no_update);
        let name = self.name_string();
        let value = &self.value;
        quote! {
            #simi_element.no_update_bool_attribute(#name, #value);
        }
    }

    fn generate_no_update_string(&self, tag: &str, simi_element: &TokenStream) -> TokenStream {
        debug_assert!(self.no_update);
        let name = self.name_string();
        let value = &self.value;
        match (tag, name.as_str()) {
            ("textarea", "value") => quote! { #simi_element.no_update_textarea_value(#value); },
            _ => {
                if self.value_is_string_literal {
                    quote! { #simi_element.no_update_literal_string_attribute(#name, #value); }
                } else {
                    quote! { #simi_element.no_update_string_attribute(#name, &#value); }
                }
            }
        }
    }

    fn generate(&self, index: usize, tag: &str, simi_element: &TokenStream) -> TokenStream {
        match self.attribute_type {
            ElementAttributeType::Unknown => {
                panic!("simi-macro bug: self.attribute_type must be known before code generating")
            }
            ElementAttributeType::Bool => self.generate_bool(index, tag, simi_element),
            ElementAttributeType::String => self.generate_string(index, tag, simi_element),
            ElementAttributeType::Index => self.generate_index(index, tag, simi_element),
        }
    }

    fn generate_bool(&self, index: usize, tag: &str, simi_element: &TokenStream) -> TokenStream {
        debug_assert!(!self.no_update);
        let name = self.name_string();
        let value = &self.value;
        match (tag, name.as_str()) {
            ("input", "checked") => {
                quote! { #simi_element.input_checked(#index, #value); }
            }
            _ => quote! { #simi_element.bool_attribute(#index, #name, #value); },
        }
    }

    fn generate_string(&self, index: usize, tag: &str, simi_element: &TokenStream) -> TokenStream {
        debug_assert!(!self.no_update);
        let name = self.name_string();
        let value = &self.value;
        match (tag, name.as_str()) {
            ("input", "value") => {
                quote! { #simi_element.input_value(#index, &#value); }
            }
            ("textarea", "value") => {
                quote! { #simi_element.textarea_value(#index, &#value); }
            }
            ("select", "value") => {
                if self.optional.is_some() {
                    quote! { #simi_element.select_optional_value(#index, &#value); }
                } else {
                    quote! { #simi_element.select_value(#index, &#value); }
                }
            }
            _ => quote! { #simi_element.string_attribute(#index, #name, &#value); },
        }
    }

    fn generate_index(&self, index: usize, tag: &str, simi_element: &TokenStream) -> TokenStream {
        debug_assert!(!self.no_update);
        let name = self.name_string();
        let value = &self.value;
        match (tag, name.as_str()) {
            ("select", "index") => {
                if self.optional.is_some() {
                    quote! { #simi_element.select_optional_index(#index, &#value); }
                } else {
                    quote! { #simi_element.select_index(#index, #value); }
                }
            }
            _ => unreachable!(),
        }
    }
}

impl ConditionalClass {
    fn generate_no_update(&self, simi_element: &TokenStream) -> TokenStream {
        debug_assert!(self.no_update);
        let class = &self.class;
        let condition = &self.condition;
        quote! {
            #simi_element.no_update_conditional_class(#class, #condition);
        }
    }

    fn generate(&self, index: usize, simi_element: &TokenStream) -> TokenStream {
        debug_assert!(!self.no_update);
        let class = &self.class;
        let condition = &self.condition;
        quote! {
            #simi_element.conditional_class(#index, #class, #condition);
        }
    }
}

impl ElementEvent {
    fn generate(&self, params: &Params, index: usize, simi_element: &TokenStream) -> TokenStream {
        let context = &params.context;

        if let ElementEventHandler::Placeholder(handler) = &self.handler {
            return quote! {
                if let Some(handler) = #handler.take() {
                    //simi::interop::console::log("placeholder handler");
                    #simi_element.event_handler(#index, &#context.main, handler);
                }
            };
        }

        let handler = self.generate_box_handler();
        quote! {
            #simi_element.event_handler(#index, &#context.main, #handler);
        }
    }

    fn generate_box_handler(&self) -> TokenStream {
        let event_name =
            super::helper::get_official_html_element_event_name(self.event_name.to_string());
        let event_type = proc_macro2::Ident::new(
            super::helper::get_js_event_argument_type(&event_name),
            proc_macro2::Span::call_site(),
        );
        let event_name = event_name[2..].to_string();
        let handler = match &self.handler {
            ElementEventHandler::EnumVariant(ev) => quote! {
               move |_: simi::interop::events::#event_type| #ev
            },
            ElementEventHandler::QuestionEnumVariant(qev) => quote! {
               move |val: simi::interop::events::#event_type| #qev(val)
            },
            ElementEventHandler::Placeholder(handler) => {
                panic!(format!(
                    "Calling generate_box_handler on a ElementEventHandler::Placeholder: {}",
                    handler
                ));
            }
        };

        quote! {
            Box::new(simi::events::#event_type::new_raw(#event_name, #handler))
        }
    }
}

impl ElementContent {
    fn generate(
        &self,
        params: &Params,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
        simi_element_node_is_new: &TokenStream,
    ) -> TokenStream {
        match self {
            ElementContent::NodeList(item) => item.generate(
                params,
                node_list,
                real_parent,
                next_sibling,
                simi_element_node_is_new,
            ),
            ElementContent::SubApp(item) => item.generate(params, node_list, real_parent),
            ElementContent::TabApps(item) => item.generate(params, node_list, real_parent),
        }
    }
}

impl SubApp {
    fn generate(
        &self,
        params: &Params,
        node_list: &TokenStream,
        real_parent: &TokenStream,
    ) -> TokenStream {
        let sub_app = params.get_item_name("sub_app");
        let id = &self.id;
        let at = &self.app_type;
        let context = &params.context;
        let sup_apps_collection_name = params.sup_apps_collection_name();
        quote! {
            let #sub_app = #node_list.sub_app::<_,#at>(#id, #real_parent, #context.main.clone());
            //if let Some(ref sub_apps) = #context.sub_apps {
            //    #sub_app.collect_to::<#at>(sub_apps);
            //}
            #sub_app.collect_to::<#at>(#sup_apps_collection_name);
        }
    }
}

impl TabApps {
    fn generate(
        &self,
        params: &Params,
        node_list: &TokenStream,
        real_parent: &TokenStream,
    ) -> TokenStream {
        let match_value = &self.match_value;
        let tab_app_arms: TokenStream = self
            .arms
            .iter()
            .map(|a| a.generate(params, node_list, real_parent))
            .collect();
        quote! {
            match #match_value {
                #tab_app_arms
            }
        }
    }
}

impl TabApp {
    fn generate(
        &self,
        params: &Params,
        node_list: &TokenStream,
        real_parent: &TokenStream,
    ) -> TokenStream {
        let match_value = &self.match_value;
        let sub_app = params.get_item_name("sub_app");
        let id = &self.sub_app.id;
        let at = &self.sub_app.app_type;
        let context = &params.context;
        let sup_apps_collection_name = params.sup_apps_collection_name();

        quote! {
            #match_value => {
                let #sub_app = #node_list.tab_sub_app::<_,#at>(#id, #real_parent, #context.main.clone());
                //if let Some(sub_apps) = #context.sub_apps {
                //    #sub_app.collect_to::<#at>(sub_apps);
                //}
                #sub_app.collect_to::<#at>(#sup_apps_collection_name);
            }
        }
    }
}

impl For {
    fn generate(
        &self,
        params: &Params,
        index: usize,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        simi_element_node_is_new: &TokenStream,
    ) -> TokenStream {
        if self.options.for_item_key.is_some() {
            self.generate_keyedfor(
                params,
                index,
                node_list,
                real_parent,
                simi_element_node_is_new,
            )
        } else {
            self.generate_nonkeyedfor(
                params,
                index,
                node_list,
                real_parent,
                simi_element_node_is_new,
            )
        }
    }

    fn generate_nonkeyedfor(
        &self,
        params: &Params,
        index: usize,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        simi_element_node_is_new: &TokenStream,
    ) -> TokenStream {
        let non_keyed_for = params.get_item_name("non_keyed_for");
        let item_count = &self.options.item_count;
        let iter_expression = &self.iter_expression;
        let for_setup = self
            .options
            .for_setup
            .as_ref()
            .map_or_else(TokenStream::new, std::clone::Clone::clone);
        let for_item_index = params.get_item_name("for_item_index");

        let create_or_update_for_item =
            self.content
                .generate_nonkeyedfor(params, &non_keyed_for, &for_item_index, real_parent);
        let create_or_update_non_keyed_for = quote! {
            let #non_keyed_for = #node_list.non_keyed_for(#index, #item_count);
            let mut #for_item_index = 0;
            for #iter_expression {
                #for_setup
                #create_or_update_for_item
                #for_item_index += 1;
            }
            #non_keyed_for.node_list_mut().truncate(#for_item_index, #real_parent);
        };
        if self.no_update {
            quote! {
                if #simi_element_node_is_new {
                    #create_or_update_non_keyed_for
                }
            }
        } else {
            create_or_update_non_keyed_for
        }
    }

    fn generate_keyedfor(
        &self,
        params: &Params,
        index: usize,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        simi_element_node_is_new: &TokenStream,
    ) -> TokenStream {
        let keyed_for = params.get_item_name("keyed_for");
        let for_hint = params.get_item_name("for_hint");
        let get_for_hint = self.options.for_hint.as_ref().map_or_else(
            || {
                quote! { let #for_hint = simi::ListChangeHint::NoHint; }
            },
            |value| {
                quote! {
                    let #for_hint = #value.get();
                    #value.set(simi::ListChangeHint::NoChange);
                }
            },
        );

        let just_do_like_non_key =
            self.generate_keyedfor_just_like_nonkeyedfor(params, &keyed_for, real_parent);
        let remove_item_only = self.generate_keyedfor_remove_only(params, &keyed_for, real_parent);
        let add_item_only = self.generate_keyedfor_add_only(params, &keyed_for, real_parent);

        let create_or_update_keyed_for = quote! {
            #get_for_hint
            if #for_hint != simi::ListChangeHint::NoChange {
                let mut #keyed_for = #node_list.keyed_for(#index);
                match #for_hint {
                    simi::ListChangeHint::NoHint |
                    simi::ListChangeHint::ItemValueChangeOnly |
                    simi::ListChangeHint::MixedChange => {
                        #just_do_like_non_key
                    }
                    simi::ListChangeHint::ItemRemoveOnly => {
                        #remove_item_only
                    }
                    simi::ListChangeHint::ItemAddOnly => {
                        #add_item_only
                    }
                    simi::ListChangeHint::NoChange => {},
                }
            }
        };

        if self.no_update {
            quote! {
                if #simi_element_node_is_new {
                    #create_or_update_keyed_for
                }
            }
        } else {
            create_or_update_keyed_for
        }
    }

    fn generate_keyedfor_just_like_nonkeyedfor(
        &self,
        params: &Params,
        keyed_for: &TokenStream,
        real_parent: &TokenStream,
    ) -> TokenStream {
        let iter_expression = &self.iter_expression;
        let for_setup = self
            .options
            .for_setup
            .as_ref()
            .map_or_else(TokenStream::new, std::clone::Clone::clone);
        let for_item_key = self.options.for_item_key.as_ref().expect("Item key");
        let for_item_index = params.get_item_name("for_item_index");

        let create_or_update_for_item = self.content.generate_keyedfor_just_like_nonkeyedfor(
            params,
            keyed_for,
            &for_item_key,
            &for_item_index,
            real_parent,
        );
        quote! {
            let mut #for_item_index = 0;
            for #iter_expression {
                #for_setup
                #create_or_update_for_item
                #for_item_index += 1;
            }
            #keyed_for.truncate(#for_item_index, #real_parent);
        }
    }

    fn generate_keyedfor_remove_only(
        &self,
        params: &Params,
        keyed_for: &TokenStream,
        real_parent: &TokenStream,
    ) -> TokenStream {
        let iter_expression = &self.iter_expression;
        let for_setup = self
            .options
            .for_setup
            .as_ref()
            .map_or_else(TokenStream::new, std::clone::Clone::clone);
        let item_key = self.options.for_item_key.as_ref().expect("Item key");
        let item_index = params.get_item_name("remove_only_item_index");

        quote! {
            let mut #item_index = 0;
            for #iter_expression {
                #for_setup
                #keyed_for.remove_if_key_mismatch(#item_index, #item_key as u64, #real_parent);
                #item_index += 1;
            }
            #keyed_for.truncate(#item_index, #real_parent);
        }
    }

    fn generate_keyedfor_add_only(
        &self,
        params: &Params,
        keyed_for: &TokenStream,
        real_parent: &TokenStream,
    ) -> TokenStream {
        let iter_expression = &self.iter_expression;
        let for_setup = self
            .options
            .for_setup
            .as_ref()
            .map_or_else(TokenStream::new, std::clone::Clone::clone);
        let item_key = self.options.for_item_key.as_ref().expect("Item key");
        let item_index = params.get_item_name("add_only_item_index");
        let create_node_if_new = self.content.generate_keyedfor_add_only(
            params,
            keyed_for,
            &item_key,
            &item_index,
            real_parent,
        );
        quote! {
            let mut #item_index = 0;
            for #iter_expression {
                #for_setup
                #create_node_if_new
                #item_index += 1;
            }
        }
    }
}

impl ForContent {
    fn generate_nonkeyedfor(
        &self,
        params: &Params,
        non_keyed_for: &TokenStream,
        for_item_index: &TokenStream,
        real_parent: &TokenStream,
    ) -> TokenStream {
        match &self {
            ForContent::Element(e) => {
                let tag = e.tag.to_string();
                let simi_element = params.get_item_name("simi_element");
                // Currently, new node is always append to the end of the list
                let next_sibling = quote! { None };
                let is_new = params.get_item_name("simi_element_is_new");
                let need_insert = params.get_item_name("need_insert");
                let update_simi_element = e.generate_init_or_update(
                    params,
                    &simi_element,
                    real_parent,
                    &next_sibling,
                    &is_new,
                    &need_insert,
                );
                quote! {
                    let (#simi_element, #is_new, #need_insert) = #non_keyed_for.element(#for_item_index, #tag);
                    {
                        #update_simi_element
                    }
                }
            }
            ForContent::Expression(e) => {
                let value = &e.value;
                quote! {
                    if let Some(text) = #non_keyed_for.text(#for_item_index, &#value) {
                        text.insert_to(#real_parent, None);
                    }
                }
            }
        }
    }

    fn generate_keyedfor_just_like_nonkeyedfor(
        &self,
        params: &Params,
        keyed_for: &TokenStream,
        for_item_key: &TokenStream,
        for_item_index: &TokenStream,
        real_parent: &TokenStream,
    ) -> TokenStream {
        match &self {
            ForContent::Element(e) => {
                let tag = e.tag.to_string();
                let simi_element = params.get_item_name("simi_element");
                // Currently, new node is always append to the end of the list by #keyed_for.element
                let next_sibling = quote! { None };
                let is_new = params.get_item_name("simi_element_is_new");
                let need_insert = params.get_item_name("need_insert");
                let update_simi_element = e.generate_init_or_update(
                    params,
                    &simi_element,
                    real_parent,
                    &next_sibling,
                    &is_new,
                    &need_insert,
                );
                quote! {
                    let(#simi_element, #is_new, #need_insert) = #keyed_for.element(#for_item_index, #for_item_key, #tag);
                    {
                        #update_simi_element
                    }
                }
            }
            ForContent::Expression(e) => {
                let value = &e.value;
                quote! {
                    if let Some(text) = #keyed_for.text(#for_item_index, #for_item_key, &#value) {
                        text.insert_to(#real_parent, None);
                    }
                }
            }
        }
    }

    fn generate_keyedfor_add_only(
        &self,
        params: &Params,
        keyed_for: &TokenStream,
        for_item_key: &TokenStream,
        for_item_index: &TokenStream,
        real_parent: &TokenStream,
    ) -> TokenStream {
        match self {
            ForContent::Element(e) => {
                let tag = e.tag.to_string();
                let simi_element = params.get_item_name("simi_element");
                let element_is_new = params.get_item_name("element_is_new");
                let next_sibling = params.get_item_name("next_sibling");
                let update_simi_element = e.generate_init_or_update(
                    params,
                    &simi_element,
                    real_parent,
                    &next_sibling,
                    &element_is_new,
                    // if let Some(...) { always insert because it is always new in this method }
                    &quote! {true},
                );
                quote! {
                    if let Some((#simi_element, #element_is_new, #next_sibling)) = #keyed_for.add_element_if_key_mismatch(#for_item_index, #for_item_key, #tag) {
                        #update_simi_element
                    }
                }
            }
            ForContent::Expression(e) => {
                let value = &e.value;
                quote! {
                    if let Some((text, next_sibling)) = #keyed_for.add_text_if_key_mismatch(#for_item_index, #for_item_key, &#value) {
                        text.insert_to(#real_parent, next_sibling);
                    }
                }
            }
        }
    }
}

impl If {
    fn generate(
        &self,
        params: &Params,
        index: usize,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
        simi_element_node_is_new: &TokenStream,
    ) -> TokenStream {
        let simi_if_arm_node = params.get_item_name("simi_if_arm");
        let if_arms: TokenStream = self
            .arms
            .iter()
            .enumerate()
            .map(|(index, arm)| {
                arm.generate(params, index, &simi_if_arm_node, real_parent, next_sibling)
            })
            .collect();
        let arm_count = self.arms.len();
        let create_or_update_final_else = self.final_else.as_ref().map_or_else(
            || {
                quote!{
                    let (next_sibling, first_render) = if #simi_if_arm_node.is_same_index(Some(#arm_count)) {
                        (None, false)
                    } else {
                        (
                            #simi_if_arm_node.remove_and_get_next_sibling(#real_parent),
                            true
                        )
                    };
                    let next_sibling = next_sibling.as_ref();
                    #simi_if_arm_node.node_list_mut().no_update_comment(
                        "A phantom comment used in place of a missing `else` if an `if-clause`",
                        #real_parent,
                        next_sibling
                    );
                    #simi_if_arm_node.set_index(#arm_count);
                }
            },
            |fe| fe.generate(params, arm_count, &simi_if_arm_node, real_parent, next_sibling),
        );

        let create_or_update_if_arm = quote! {
            let #simi_if_arm_node = #node_list.arm(#index);
            #if_arms
            else {
                #create_or_update_final_else
            }
        };

        if self.no_update {
            quote! {
                if #simi_element_node_is_new {
                    #create_or_update_if_arm
                }
            }
        } else {
            create_or_update_if_arm
        }
    }
}

impl IfArm {
    fn generate(
        &self,
        params: &Params,
        index: usize,
        simi_if_arm_node: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
    ) -> TokenStream {
        let condition = &self.condition;
        let create_or_update_if_arm =
            self.body
                .generate(params, index, simi_if_arm_node, real_parent, next_sibling);
        if index == 0 {
            quote! {
                if #condition {
                    #create_or_update_if_arm
                }
            }
        } else {
            quote! {
                else if #condition {
                    #create_or_update_if_arm
                }
            }
        }
    }
}

impl ArmBody {
    fn generate(
        &self,
        params: &Params,
        index: usize,
        simi_if_node: &TokenStream,
        real_parent: &TokenStream,
        outter_next_sibling: &TokenStream,
    ) -> TokenStream {
        let node_list = params.get_item_name("if_arm_node_list");
        let next_sibling = params.get_item_name("if_arm_next_sibling");
        let first_render = params.get_item_name("if_arm_first_render");
        let create_or_update_if_arm = self.node_list.generate(
            params,
            &node_list,
            real_parent,
            &next_sibling,
            &first_render,
        );
        quote! {
            let (#next_sibling, #first_render) = if #simi_if_node.is_same_index(Some(#index)) {
                (None, false)
            } else {
                (
                    #simi_if_node.remove_and_get_next_sibling(#real_parent),
                    true
                )
            };
            let #next_sibling = #next_sibling.as_ref().or(#outter_next_sibling);
            let #node_list = #simi_if_node.node_list_mut();
            #create_or_update_if_arm
            #simi_if_node.set_index(#index);
        }
    }
}

impl Match {
    fn generate(
        &self,
        params: &Params,
        index: usize,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
        simi_element_node_is_new: &TokenStream,
    ) -> TokenStream {
        let simi_match_arm_node = params.get_item_name("simi_match_arm");
        let match_arms: TokenStream = self
            .arms
            .iter()
            .enumerate()
            .map(|(index, arm)| {
                arm.generate(
                    params,
                    index,
                    &simi_match_arm_node,
                    real_parent,
                    next_sibling,
                )
            })
            .collect();

        let match_value = &self.match_value;

        let create_or_update_match_arm = quote! {
            let #simi_match_arm_node = #node_list.arm(#index);
            match #match_value {
                #match_arms
            }
        };

        if self.no_update {
            quote! {
                if #simi_element_node_is_new {
                    #create_or_update_match_arm
                }
            }
        } else {
            create_or_update_match_arm
        }
    }
}

impl MatchArm {
    fn generate(
        &self,
        params: &Params,
        index: usize,
        simi_match_arm_node: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
    ) -> TokenStream {
        let pattern = &self.pattern;
        let create_or_update_match_arm = self.body.generate(
            params,
            index,
            simi_match_arm_node,
            real_parent,
            next_sibling,
        );
        quote! {
            #pattern => {
                #create_or_update_match_arm
            }
        }
    }
}

impl Component {
    fn generate(
        &self,
        params: &Params,
        index: usize,
        node_list: &TokenStream,
        real_parent: &TokenStream,
        next_sibling: &TokenStream,
    ) -> TokenStream {
        let component_node = params.get_item_name("component_node");
        let component_first_render = params.get_item_name("first_render");
        let context = &params.context;
        let component_context = params.get_item_name("component_context");
        let component_instance = params.get_item_name("component_instance");
        let init_component = self.init_component(&component_first_render);

        quote! {{
            let (#component_first_render, #component_node) = #node_list.component(#index);
            let #component_context = RenderContext::new(
                #component_node.node_list_mut(),
                #real_parent,
                #context.main.clone(),
                #next_sibling
            );
            let #component_instance = #init_component;
            #component_instance.render(#component_context);
        }}
    }

    fn init_component(&self, first_render: &TokenStream) -> TokenStream {
        let regular_fields: TokenStream = self
            .regular_fields
            .iter()
            .map(crate::render::ComponentRegularField::generate)
            .collect();
        let init_events: TokenStream = self
            .events
            .iter()
            .map(|e| e.generate(first_render))
            .collect();
        let init_childs: TokenStream = self
            .childs
            .iter()
            .map(|cc| cc.generate(first_render))
            .collect();
        let component_other_fields = self.component_other_fields();
        let component_type = &self.component_type;
        quote! {{
            #init_events
            #init_childs
            #component_type {
                #regular_fields
                #component_other_fields
            }
        }}
    }

    fn component_other_fields(&self) -> TokenStream {
        let event_fields: TokenStream = self
            .events
            .iter()
            .map(|e| {
                let id = &e.id;
                quote! {
                    #id,
                }
            })
            .collect();
        let child_fields: TokenStream = self
            .childs
            .iter()
            .map(|cc| {
                let id = &cc.id;
                quote! { #id, }
            })
            .collect();
        quote! {
            #event_fields
            #child_fields
        }
    }
}

impl ComponentRegularField {
    fn generate(&self) -> TokenStream {
        let id = &self.id;
        let value = &self.value;
        quote! {
            #id: #value,
        }
    }
}

impl ComponentEvent {
    fn generate(&self, first_render: &TokenStream) -> TokenStream {
        let id = &self.id;
        let handler = self.event.generate_box_handler();
        if self.event.no_update {
            quote! {
                let #id: simi::events::ElementEventHandler<_> = if #first_render {
                    std::cell::Cell::new(Some(#handler))
                } else {
                    std::cell::Cell::new(None)
                };
            }
        } else {
            quote! {
                let #id: simi::events::ElementEventHandler<_> = std::cell::Cell::new(Some(#handler));
            }
        }
    }
}

impl ChildComponent {
    fn generate(&self, first_render: &TokenStream) -> TokenStream {
        let id = &self.id;
        let init_component = self.comp.init_component(first_render);
        quote! {
            //let #id: std::cell::Cell<Option<Box<Component<'_,_>>>> = std::cell::Cell::new(Some(Box::new(#init_component)));
            let #id = Box::new(#init_component);
        }
    }
}
