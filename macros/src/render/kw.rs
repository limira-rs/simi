use syn::custom_keyword;

custom_keyword!(component);
custom_keyword!(sub_app);
custom_keyword!(tab_apps);
