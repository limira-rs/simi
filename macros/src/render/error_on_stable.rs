/// Help reporting errors on stable Rust

// This is temporary because I need a quick solution
// TODO: investigate using quote::quote_spanned!??
pub fn panic_with(message: &str, item: &impl std::fmt::Display, span: proc_macro2::Span) {
    panic!(format!(
        "{}
=====
'{}' appears on line {}
=====
{}
=====
        ",
        message,
        item,
        span.start().line,
        crate::BETTER_ERROR_MESSAGE
    ))
}