use std::iter::FromIterator;
use syn::parse::{Error, Parse, ParseStream, Result};
use syn::spanned::Spanned;
use syn::{braced, parenthesized, token, LitStr};

use super::*;

type NoUpdateSymbol = Option<token::Pound>;
//type PlaceholderSymbol = Option<token::Dollar>;
pub type OptionalSymbol = Option<token::Question>;

struct AtSemiPair {
    ident: syn::Ident,
    value: TokenStream,
}

impl Parse for AtSemiPair {
    fn parse(input: ParseStream) -> Result<Self> {
        let _: token::At = input.parse()?;
        let ident: syn::Ident = input.parse()?;
        let _: token::Eq = input.parse()?;
        if input.peek(token::Brace) {
            let group: proc_macro2::Group = input.parse().expect("Must ok");
            return Ok(AtSemiPair {
                ident,
                value: group.stream(),
            });
        }
        let mut value = TokenStream::new();
        while !input.is_empty() && !input.peek(token::Semi) {
            let tt: TokenTree = input.parse()?;
            value.extend(Some(tt));
        }
        if !input.is_empty() {
            let _: token::Semi = input.parse()?;
        }
        Ok(AtSemiPair { ident, value })
    }
}

// How to avoid code duplication in CommaedTokenStream/EndByBraceTokenStream...
struct CommaedTokenStream {
    stream: TokenStream,
}

impl Parse for CommaedTokenStream {
    fn parse(input: ParseStream) -> Result<Self> {
        let mut stream = TokenStream::new();
        while !input.is_empty() && !input.peek(token::Comma) {
            let tt: TokenTree = input.parse()?;
            stream.extend(Some(tt));
        }
        if !input.is_empty() {
            let _: token::Comma = input.parse()?;
        }
        Ok(Self { stream })
    }
}

struct EndByBraceTokenStream {
    stream: TokenStream,
}

impl Parse for EndByBraceTokenStream {
    fn parse(input: ParseStream) -> Result<Self> {
        let mut stream = TokenStream::new();
        while !input.is_empty() && !input.peek(token::Brace) {
            let tt: TokenTree = input.parse()?;
            stream.extend(Some(tt));
        }
        Ok(Self { stream })
    }
}

struct IncludedBraceTokenStream {
    stream: TokenStream,
}

impl Parse for IncludedBraceTokenStream {
    fn parse(input: ParseStream) -> Result<Self> {
        let mut stream = TokenStream::new();
        while !input.is_empty() && !input.peek(token::Brace) {
            let tt: TokenTree = input.parse()?;
            stream.extend(Some(tt));
        }
        if !input.is_empty() {
            stream.extend(Some(input.parse::<TokenTree>()?));
        }
        Ok(Self { stream })
    }
}

#[derive(Debug)]
enum ComponentFieldType {
    Regular,
    Event,
    Component,
}
struct ComponentField {
    field_type: ComponentFieldType,
    id: Ident,
    stream: TokenStream,
}

impl Parse for ComponentField {
    fn parse(input: ParseStream) -> Result<Self> {
        let id: Ident = input.parse()?;
        let _: token::Colon = input.parse()?;
        let field_type = if input.peek(Ident) {
            let input = input.fork();
            let ident = input.parse::<Ident>()?.to_string();
            if input.peek(token::Eq) {
                ComponentFieldType::Event
            } else if ident.chars().next().unwrap().is_uppercase() {
                ComponentFieldType::Component
            } else {
                ComponentFieldType::Regular
            }
        } else {
            ComponentFieldType::Regular
        };
        let stream = match field_type {
            ComponentFieldType::Regular | ComponentFieldType::Event => {
                let s: CommaedTokenStream = input.parse()?;
                s.stream
            }
            ComponentFieldType::Component => {
                let s: IncludedBraceTokenStream = input.parse()?;
                if !input.is_empty() && input.peek(token::Comma) {
                    let _: token::Comma = input.parse()?;
                }
                s.stream
            }
        };
        Ok(ComponentField {
            field_type,
            id,
            stream,
        })
    }
}

impl Parse for MacroInput {
    fn parse(input: ParseStream) -> Result<Self> {
        let options: MacroOptions = if input.peek(token::At) {
            input.parse()?
        } else {
            MacroOptions::default()
        };

        if input.is_empty() {
            return Err(input.error("expected at least one item"));
        }
        let node_list: NodeList = input.parse()?;
        Ok(Self { options, node_list })
    }
}

impl Parse for MacroOptions {
    fn parse(input: ParseStream) -> Result<Self> {
        const WANTED: &str = "Expected `debug`";
        let mut options = Self::default();

        loop {
            let _at: token::At = input.parse()?;
            let ident: Ident = input.parse()?;
            match ident.to_string().as_ref() {
                "debug" => options.debug = true,
                "app_type" => {
                    let _: token::Eq = input.parse()?;
                    let app_type: Ident = input.parse()?;
                    options.app_type = Some(app_type);
                }
                "context" => {
                    let _: token::Eq = input.parse()?;
                    let context: Ident = input.parse()?;
                    options.context = Some(context);
                }
                _ => return Err(Error::new(ident.span(), WANTED)),
            }
            if !input.peek(Token![@]) {
                break;
            }
        }

        Ok(options)
    }
}

impl Parse for NodeList {
    fn parse(input: ParseStream) -> Result<Self> {
        const WANTED: &str = r#"Expected
    input (...)
    div {
        ...
    }
    ComponentType {
        regular_field: value,
        event_field: onclick = Msg::ButtonClick,
        child_field_1: AnotherComponentType {
            a_field: some_value,
        }
    }
    div {
        @sub_app(unique_app_id, AppType)
    }
    div {
        @tab_apps self.match_value {
            match_pattern_1 {
                @sub_app(unique_app_id, AppType)
            }
            match_pattern_2 {
                @sub_app(unique_app_id, AppType)
            }
        }
    }
    (all - some_value)
    some_value
    self.method_call()
    self.field_name
    "Literal"
An expression may be prefixed with `#` to exclude it from updating.
"#;
        let mut nodes = Vec::new();
        while !input.is_empty() {
            let ahead = input.fork();
            let _: NoUpdateSymbol = ahead.parse()?;
            // if
            // match
            // for
            //

            // If next token is an Ident
            //      ? html tags
            //      ? first token of a value accessor: a struct field or a method/function call
            //      ? a component name
            let tt: TokenTree = ahead.parse()?;
            match &tt {
                TokenTree::Ident(next_ident) => {
                    let next_ident = next_ident.to_string();
                    if helper::is_html_element_tag(&next_ident) {
                        // a supported html tag
                        nodes.push(Node::Element(input.parse()?));
                    } else if next_ident.chars().next().unwrap().is_uppercase() {
                        // component name is camel case
                        nodes.push(Node::Component(input.parse()?));
                    } else {
                        match next_ident.as_ref() {
                            "if" => nodes.push(Node::If(input.parse()?)),
                            "match" => nodes.push(Node::Match(input.parse()?)),
                            "for" => {
                                nodes.push(Node::For(input.parse()?));
                                if !input.is_empty() {
                                    return Err(input.error("Simi requires `for-loop` must be the last item in the content list. This item is not allowed to come right after the previous `for-loop`"));
                                }
                            }
                            _ => nodes.push(Node::Expression(input.parse()?)),
                        }
                    }
                }
                TokenTree::Literal(_) => {
                    nodes.push(Node::Literal(input.parse()?));
                }
                TokenTree::Group(g) => {
                    if g.delimiter() == proc_macro2::Delimiter::Parenthesis {
                        let no_update = input.parse::<NoUpdateSymbol>()?.is_some();
                        let tt: TokenTree = input.parse()?;
                        let s = TokenStream::from_iter(Some(tt));
                        nodes.push(Node::Expression(SimpleExpression {
                            no_update,
                            value: s,
                            optional: None,
                        }));
                    } else {
                        return Err(
                            input.error(format!("Not expected [] or {{}} here.\n{}", WANTED))
                        );
                    }
                }
                token => {
                    match token.to_string().as_str() {
                        "@" => {
                            // An element content must be parsed by ElementContent first
                            // Therefore, `@` should never be encountered here in the NodeList
                            return Err(input.error("Unexpected token. Items such as `@sub_app` or `@tab_apps` must be the only item in an element content"));
                        }
                        "$" => {
                            let _: token::Dollar = input.parse()?;
                            let s: SimpleExpression = input.parse()?;
                            nodes.push(Node::ComponentPlaceholder(s.value));
                        }

                        _ => return Err(input.error(WANTED)),
                    }
                }
            }
        }

        Ok(Self { nodes })
    }
}

impl Parse for Element {
    fn parse(input: ParseStream) -> Result<Self> {
        let no_update = input.parse::<NoUpdateSymbol>()?.is_some();
        let tag: Ident = input.parse()?;
        let (no_attributes, attributes) = if input.peek(token::Paren) {
            let attributes;
            parenthesized!(attributes in input);
            let a = if attributes.is_empty() {
                None
            } else {
                Some(attributes.parse::<ElementAttributes>()?)
            };
            (false, a)
        } else {
            (true, None)
        };
        let (no_content, content) = if input.peek(token::Brace) {
            if tag == "textarea" {
                return Err(input
                    .error("In Simi, you must assign textarea's content via attribute `value` instead of providing its content in this brace"));
            }
            if !helper::has_body_content(&tag.to_string()) {
                return Err(input.error(&format!("{} not accept a body content", tag)));
            }
            let content;
            braced!(content in input);
            let c = if content.is_empty() {
                None
            } else {
                Some(content.parse::<ElementContent>()?)
            };
            (false, c)
        } else {
            (true, None)
        };
        if no_attributes && no_content {
            match tag.to_string().as_ref() {
                "hr" | "br" => {}
                _ => {
                    return Err(Error::new(
                        tag.span(),
                        format!("`{}`: elements other than `hr` or `br` must come with `()`, or `{{}}`, or both `() {{}}`",tag)
                    ))
                }
            }
        }

        let mut e = Self {
            no_update,
            tag,
            attributes,
            content,
        };
        if !cfg!(test) {
            e.validate_element_attribute();
        }
        Ok(e)
    }
}

impl Parse for ElementAttributes {
    fn parse(input: ParseStream) -> Result<Self> {
        let mut attributes: Vec<ElementAttribute> = Vec::new();
        let mut no_update_attributes: Vec<ElementAttribute> = Vec::new();
        let mut literal_classes: Vec<LitStr> = Vec::new();
        let mut no_update_classes: Vec<ConditionalClass> = Vec::new();
        let mut classes: Vec<ConditionalClass> = Vec::new();
        let mut no_update_events: Vec<ElementEvent> = Vec::new();
        let mut events: Vec<ElementEvent> = Vec::new();
        while !input.is_empty() {
            if let Some(ident) = crate::peek_next_ident(input)? {
                let name = ident.to_string();
                if helper::is_html_element_event(&name) {
                    let e: ElementEvent = input.parse()?;
                    if e.no_update {
                        no_update_events.push(e);
                    } else {
                        events.push(e);
                    }
                } else if name == "class" {
                    let _: Ident = input.parse()?;
                    let _: token::Eq = input.parse()?;
                    let lit: LitStr = input.parse()?;
                    literal_classes.push(lit);
                } else {
                    let a: ElementAttribute = input.parse()?;
                    if a.no_update {
                        no_update_attributes.push(a);
                    } else {
                        attributes.push(a);
                    }
                }
            } else if input.peek(LitStr) {
                let c: ConditionalClass = input.parse()?;
                if c.no_update {
                    no_update_classes.push(c);
                } else {
                    classes.push(c);
                }
            } else {
                return Err(input.error("Expected an html element attribute or a string literal"));
            }
        }
        Ok(Self {
            attributes,
            no_update_attributes,
            classes,
            literal_classes,
            no_update_classes,
            events,
            no_update_events,
        })
    }
}

impl Parse for ElementEvent {
    fn parse(input: ParseStream) -> Result<Self> {
        let event_name: Ident = input.fork().parse()?;
        if !helper::is_html_element_event(&event_name.to_string()) {
            return Err(input.error("Unsupported element event"));
        }
        let event_name: Ident = input.parse()?;
        let _: token::Eq = input.parse()?;
        let no_update = input.parse::<NoUpdateSymbol>()?.is_some();
        let handler: ElementEventHandler = input.parse()?;

        Ok(Self {
            no_update,
            event_name,
            handler,
        })
    }
}

impl Parse for ElementEventHandler {
    fn parse(input: ParseStream) -> Result<Self> {
        let mut handler = TokenStream::new();
        loop {
            let tt: TokenTree = input.parse()?;
            let is_ident = if let TokenTree::Ident(ref _not_use) = tt {
                true
            } else {
                false
            };
            handler.extend(Some(tt));
            if input.is_empty() {
                if handler.to_string().starts_with("self .") {
                    return Ok(ElementEventHandler::Placeholder(handler));
                } else {
                    return Ok(ElementEventHandler::EnumVariant(handler));
                }
            } else if input.peek(token::Paren) {
                let paren_group: TokenTree = input.parse()?;
                if paren_group.to_string() == "( ? )" {
                    return Ok(ElementEventHandler::QuestionEnumVariant(handler));
                } else {
                    // For example: Msg::ClickedItem(index)
                    handler.extend(Some(paren_group));
                    return Ok(ElementEventHandler::EnumVariant(handler));
                }
            } else if is_ident && input.peek(Ident) {
                return Ok(ElementEventHandler::EnumVariant(handler));
            }
        }
    }
}

impl Parse for ElementAttribute {
    fn parse(input: ParseStream) -> Result<Self> {
        const WANTED: &str = r#"Expected html element attributes like
div (
    id = "some-id"
    class = "class-name1 class-name2"
    "conditional-class" = bool_value_for_conditional_class
    onclick = Msg::MessageVariant1
    onchange = Msg::MessageVariant2(?)
    data-custom-attribute = some_value
)"#;
        let mut name = TokenStream::new();
        let mut require_ident_next = true;
        let mut custom = false;
        loop {
            let tt: TokenTree = input.parse()?;
            name.extend(Some(tt));
            require_ident_next = !require_ident_next;
            if input.peek(token::Eq) {
                break;
            } else if input.peek(Ident) {
                if !require_ident_next {
                    return Err(input.error(WANTED));
                }
            } else if input.peek(token::Sub) {
                if require_ident_next {
                    return Err(input.error(WANTED));
                }
                custom = true;
            } else {
                return Err(input.error(WANTED));
            }
        }
        let _: token::Eq = input.parse()?;
        let SimpleExpression {
            no_update,
            value,
            optional,
        } = input.parse::<SimpleExpression>()?;
        let value_is_string_literal = match syn::parse2::<LitStr>(value.clone()) {
            Ok(_) => true,
            _ => false,
        };

        Ok(Self {
            no_update,
            optional,
            custom,
            name,
            value,
            value_is_string_literal,
            attribute_type: ElementAttributeType::Unknown,
        })
    }
}

impl Parse for SimpleExpression {
    fn parse(input: ParseStream) -> Result<Self> {
        let no_update = input.parse::<NoUpdateSymbol>()?.is_some();
        let optional = input.parse::<OptionalSymbol>()?;
        // If no_update is already true, input.parse will not be executed
        let no_update = no_update || input.parse::<NoUpdateSymbol>()?.is_some();

        let mut value: Vec<TokenTree> = Vec::new();
        while !input.is_empty() {
            let tt: TokenTree = input.parse()?;
            let expect_ident_or_literal = match &tt {
                TokenTree::Ident(ident) => ident == "as",
                TokenTree::Group(_) => false,
                TokenTree::Punct(_) => true,
                TokenTree::Literal(_) => false,
            };
            value.push(tt);
            if input.is_empty()
                || input.peek(token::Pound)
                || input.peek(token::At)
                || input.peek(token::Dollar)
            {
                break;
            }
            if !expect_ident_or_literal {
                if input.peek(LitStr) {
                    break;
                }
                if crate::peek_next_ident(input)?.is_some() {
                    break;
                }
            }
        }
        let no_update = no_update
            || if value.len() == 1 {
                match &value[0] {
                    TokenTree::Literal(_) => true,
                    TokenTree::Ident(ident) if ident == "true" || ident == "false" => true,
                    _ => false,
                }
            } else {
                false
            };
        let value = TokenStream::from_iter(value);
        //println!("simple value: {} => update: {}", value, !no_update);
        Ok(Self {
            no_update,
            value,
            optional,
        })
    }
}

impl Parse for ConditionalClass {
    fn parse(input: ParseStream) -> Result<Self> {
        let class: LitStr = input.parse()?;
        let _: token::Eq = input.parse()?;
        let SimpleExpression {
            no_update,
            value: condition,
            ..
        } = input.parse::<SimpleExpression>()?;
        Ok(Self {
            no_update,
            class,
            condition,
        })
    }
}

impl Parse for ElementContent {
    fn parse(input: ParseStream) -> Result<Self> {
        const WANTED: &str = r#"Expect 
    @sub_app(id, AppType)
    or
    @tab_apps some_value {
        Match::Pattern1 {
            @sub_app(id1, AppType1)
        }
        Match::Pattern2 {
            @sub_app(id2, AppType2)
        }
    }
"#;
        if input.peek(token::At) {
            if input.peek2(kw::sub_app) {
                Ok(ElementContent::SubApp(input.parse()?))
            } else if input.peek2(kw::tab_apps) {
                Ok(ElementContent::TabApps(input.parse()?))
            } else {
                Err(input.error(WANTED))
            }
        } else {
            Ok(ElementContent::NodeList(input.parse()?))
        }
    }
}

impl Parse for SubApp {
    fn parse(input: ParseStream) -> Result<Self> {
        let _: token::At = input.parse()?;
        let kw: kw::sub_app = input.parse()?;
        let paren;
        parenthesized!(paren in input);
        let id = paren.parse::<CommaedTokenStream>()?.stream;
        let app_type = paren.parse::<CommaedTokenStream>()?.stream;
        if !input.is_empty() {
            return Err(input.error(
                "Unexpected token. @sub_app is expected to be the only content in an element",
            ));
        }
        Ok(Self { kw, id, app_type })
    }
}

impl Parse for TabApps {
    fn parse(input: ParseStream) -> Result<Self> {
        let _: token::At = input.parse()?;
        let kw: kw::tab_apps = input.parse()?;
        let match_value = input.parse::<EndByBraceTokenStream>()?.stream;
        let apps;
        braced!(apps in input);
        let input = apps;
        let mut arms = Vec::new();
        while !input.is_empty() {
            arms.push(input.parse()?);
        }
        Ok(Self {
            kw,
            match_value,
            arms,
        })
    }
}

impl Parse for TabApp {
    fn parse(input: ParseStream) -> Result<Self> {
        let match_value = input.parse::<EndByBraceTokenStream>()?.stream;
        let sub_app;
        braced!(sub_app in input);
        let sub_app = sub_app.parse()?;
        Ok(Self {
            match_value,
            sub_app,
        })
    }
}

impl Parse for For {
    fn parse(input: ParseStream) -> Result<Self> {
        let no_update = input.parse::<NoUpdateSymbol>()?.is_some();
        let for_kw: token::For = input.parse()?;
        let mut iter_expression = TokenStream::new();
        loop {
            let tt: TokenTree = input.parse()?;
            iter_expression.extend(Some(tt));
            if input.peek(token::Brace) {
                break;
            }
        }
        let for_body;
        braced!(for_body in input);
        let options: ForOptions = for_body.parse()?;

        if options.for_hint.is_some() && options.for_item_key.is_none() {
            return Err(Error::new(
                for_kw.span(),
                "[Inside this loop] @for_hint can not be specified without @for_item_key",
            ));
        }
        //println!("Start parsing body");
        let mut content: NodeList = for_body.parse()?;
        //println!("Start parsing body... DONE");
        if content.nodes.len() != 1 {
            return Err(Error::new(
                for_kw.span(),
                "Exactly one item is allowed at the root of the body of a `for-loop`",
            ));
        }
        let content = match content.nodes.pop().unwrap() {
            Node::Element(e) => {
                e.inspect_content_of_for_loop();
                ForContent::Element(e)
            },
            Node::Expression(e) => ForContent::Expression(e),
            _ => {
                return Err(Error::new(
                    for_kw.span(),
                    "Only html element or an expression is allow at the root of the body of a `for-loop`",
                ))
            }
        };
        Ok(Self {
            no_update,
            options,
            iter_expression,
            content,
        })
    }
}

impl Parse for ForOptions {
    fn parse(input: ParseStream) -> Result<Self> {
        let mut item_count = None;
        let mut for_hint = None;
        let mut for_setup = None;
        let mut for_item_key = None;
        while input.peek(token::At) {
            let atpair: AtSemiPair = input.parse()?;
            match atpair.ident.to_string().as_str() {
                "item_count" => item_count = Some(atpair.value),
                "for_hint" => for_hint = Some(atpair.value),
                "for_setup" => {
                    let g: Result<proc_macro2::Group> = syn::parse2(atpair.value.clone());
                    match g {
                        Ok(g) => {
                            if g.delimiter() == proc_macro2::Delimiter::Brace {
                                for_setup = Some(g.stream())
                            } else {
                                return Err(Error::new(g.span(), "Expected @for_setup = { rust's expressions in a brace }"));
                            }
                        }
                        Err(_) => for_setup = Some(atpair.value),
                    }
                }
                "for_item_key" => for_item_key = Some(atpair.value),
                _=>
                return Err(Error::new(atpair.ident.span(), "`for-loop` expects `@item_count=some_value`, `@for_hint=self.hint`, `@for_item_key=item.key` or `@for_setup={}`")),
            }
        }
        if item_count.is_none() {
            return Err(input.error(r#"Unexpected item. Simi requires you to provide an @item_count value like this:
                for item in items.iter() {

                    @item_count=some_value; // <= Required by simi

                    // And then your code for the loop goes here
                }"#
            ));
        }
        let rs = ForOptions {
            item_count: item_count.unwrap(),
            for_hint,
            for_setup,
            for_item_key,
        };
        Ok(rs)
    }
}

impl Parse for If {
    fn parse(input: ParseStream) -> Result<Self> {
        let no_update = input.parse::<NoUpdateSymbol>()?.is_some();
        let arm: IfArm = input.parse()?;
        let mut arms = vec![arm];
        while input.peek(token::Else) {
            let _: token::Else = input.parse()?;
            if input.peek(token::If) {
                arms.push(input.parse()?);
            } else {
                let final_else;
                braced!(final_else in input);
                if final_else.is_empty() {
                    return Err(final_else.error(
                        "Expected at least one item such as: an html element, an expression...",
                    ));
                }

                let node_list: NodeList = final_else.parse()?;
                return Ok(Self {
                    no_update,
                    arms,
                    final_else: Some(ArmBody { node_list }),
                });
            }
        }
        Ok(Self {
            no_update,
            arms,
            final_else: None,
        })
    }
}

impl Parse for IfArm {
    fn parse(input: ParseStream) -> Result<Self> {
        let _: token::If = input.parse()?;
        let mut condition = TokenStream::new();
        while !input.peek(token::Brace) {
            let tt: TokenTree = input.parse()?;
            condition.extend(Some(tt));
        }
        let body;
        braced!(body in input);
        if body.is_empty() {
            return Err(
                body.error("Expected at least one item such as: an html element, an expression...")
            );
        }
        let node_list: NodeList = body.parse()?;
        Ok(Self {
            condition,
            body: ArmBody { node_list },
        })
    }
}

impl Parse for Match {
    fn parse(input: ParseStream) -> Result<Self> {
        let no_update = input.parse::<NoUpdateSymbol>()?.is_some();
        let _: token::Match = input.parse()?;
        let mut match_value = TokenStream::new();
        while !input.peek(token::Brace) {
            let tt: TokenTree = input.parse()?;
            match_value.extend(Some(tt));
        }
        let body;
        braced!(body in input);
        let mut arms = Vec::new();
        while !body.is_empty() {
            arms.push(body.parse()?);
        }
        Ok(Self {
            no_update,
            arms,
            match_value,
        })
    }
}

impl Parse for MatchArm {
    fn parse(input: ParseStream) -> Result<Self> {
        let mut pattern = TokenStream::new();
        while !input.peek(token::FatArrow) {
            let tt: TokenTree = input.parse()?;
            pattern.extend(Some(tt));
        }
        let farrow: token::FatArrow = input.parse()?;
        if input.peek(token::Brace) {
            let body;
            braced!(body in input);
            let node_list: NodeList = body.parse()?;
            Ok(Self {
                pattern,
                body: ArmBody { node_list },
            })
        } else {
            let mut a_single_item = TokenStream::new();
            while !input.is_empty() {
                if input.peek(token::Comma) {
                    let _: token::Comma = input.parse()?;
                    break;
                }
                let tt: TokenTree = input.parse()?;
                a_single_item.extend(Some(tt));
            }
            let node_list: NodeList = syn::parse2(a_single_item)?;
            if node_list.nodes.len() != 1 {
                return Err(Error::new(farrow.span(), "If you want to have multiple item you must put them in {} like: ` => { item1 item2 }`"));
            }
            Ok(Self {
                pattern,
                body: ArmBody { node_list },
            })
        }
    }
}

impl Parse for Component {
    fn parse(input: ParseStream) -> Result<Self> {
        let no_update = input.parse::<NoUpdateSymbol>()?.is_some();
        let component_type: Ident = input.parse()?;

        let mut events = Vec::new();
        let mut childs = Vec::new();
        let mut regular_fields = Vec::new();
        if input.peek(token::Brace) {
            let fields;
            braced! (fields in input);

            while !fields.is_empty() {
                let ComponentField {
                    id,
                    field_type,
                    stream,
                } = fields.parse()?;

                match field_type {
                    ComponentFieldType::Regular => {
                        regular_fields.push(ComponentRegularField { id, value: stream });
                    }
                    ComponentFieldType::Event => {
                        let event: ElementEvent = syn::parse2(stream)?;
                        events.push(ComponentEvent { id, event });
                    }
                    ComponentFieldType::Component => {
                        let comp: Component = syn::parse2(stream)?;
                        childs.push(ChildComponent { id, comp });
                    }
                }
            }
        } else {
            return Err(input.error(format!(
                "A component expect a `{{}}` after its type name like: `{} {{}}`",
                component_type
            )));
        }

        Ok(Self {
            no_update,
            component_type,
            regular_fields,
            events,
            childs,
        })
    }
}
