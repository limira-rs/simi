use quote::quote;
use syn::parse::{Parse, ParseStream, Result};
use syn::LitStr;

pub enum SimiAppContainer {
    DocumentBody,
    ElementId(LitStr),
}

impl Parse for SimiAppContainer {
    fn parse(input: ParseStream) -> Result<Self> {
        const WANTED: &str = "Expected one of: #[simi_app] or #[simi_app(element-id)] or #[simi_app(\"element-id\")]";
        if input.is_empty() {
            return Ok(SimiAppContainer::DocumentBody);
        }
        if input.peek(LitStr) {
            let id: LitStr = input.parse()?;
            if !input.is_empty() {
                return Err(input.error(format!("Unexpected token. {}", WANTED)));
            }
            return Ok(SimiAppContainer::ElementId(id));
        }
        if crate::peek_next_ident(input)?.is_none() {
            return Err(input.error(format!("Unexpected token. {}", WANTED)));
        }
        let mut id = String::new();
        while !input.is_empty() {
            let tt: proc_macro2::TokenTree = input.parse()?;
            id.push_str(&tt.to_string());
        }
        Ok(SimiAppContainer::ElementId(LitStr::new(
            &id,
            proc_macro2::Span::call_site(),
        )))
    }
}

#[cfg(not(feature="nightly"))]
fn emit_error(item: &syn::Item) -> ! {
    let item = quote!{#item};
    panic!("The app state must be either a `struct` or an `enum` while your item is: \n----\n{}\n----\n{}", item, crate::BETTER_ERROR_MESSAGE);
}

#[cfg(feature="nightly")]
fn emit_error(item: &syn::Item) -> ! {
    use syn::spanned::Spanned;
    item.span()
        .unstable()
        .error("Unexpected Rust's item. #[simi_app] expected only `struct` or `enum` for the app state")
        .emit();
    panic!("Stop because of previous error")
}

pub fn impl_app_handle(
    simi_app_container: SimiAppContainer,
    app_state_item: &syn::Item,
    item_input: &proc_macro2::TokenStream,
) -> proc_macro2::TokenStream {
    let app_state_type = match &app_state_item {
        syn::Item::Struct(item) => item.ident.clone(),
        syn::Item::Enum(item) => item.ident.clone(),
        item => emit_error(item),
    };

    //let item_input: proc_macro2::TokenStream = item_input.into();

    let start = match simi_app_container {
        SimiAppContainer::DocumentBody => {
            quote! {
                RcMain::start_in_body(())
            }
        }
        SimiAppContainer::ElementId(id) => {
            quote! {
                RcMain::start_in_element_id(#id, ())
            }
        }
    };

    quote! {
        #item_input

        #[wasm_bindgen]
        pub struct AppHandle {
            main: RcMain<#app_state_type>,
        }

        #[wasm_bindgen]
        impl AppHandle {
            #[wasm_bindgen(constructor)]
            pub fn new() -> Self {
                Self {
                    main: #start,
                }
            }
        }
    }
}
