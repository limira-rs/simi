#![recursion_limit = "128"]
#![cfg_attr(feature="nightly", feature(proc_macro_diagnostic))]

extern crate proc_macro;

use quote::quote;
use syn::parse_macro_input;
#[cfg(not(feature="nightly"))]
use proc_macro_hack::proc_macro_hack;

mod render;
mod simi_app;

#[cfg(not(feature="nightly"))]
const BETTER_ERROR_MESSAGE: &str = "If you want a better error message, enable the feature `nightly` for the development process (only build release on stable Rust)";

fn peek_next_ident(input: syn::parse::ParseStream) -> syn::parse::Result<Option<syn::Ident>> {
    if let proc_macro2::TokenTree::Ident(ident) = input.fork().parse()? {
        Ok(Some(ident))
    } else {
        Ok(None)
    }
}

#[proc_macro_attribute]
pub fn simi_app(
    element_id: proc_macro::TokenStream,
    item_input: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let simi_app_container = parse_macro_input!(element_id as simi_app::SimiAppContainer);
    let item = item_input.clone();
    let item = parse_macro_input!(item as syn::Item);
    simi_app::impl_app_handle(simi_app_container, &item, &item_input.into()).into()
}

#[cfg_attr(feature="nightly", proc_macro)]
#[cfg_attr(not(feature="nightly"), proc_macro_hack)]
pub fn application(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    render(input, crate::render::RenderMode::Application)
}

#[cfg_attr(feature="nightly", proc_macro)]
#[cfg_attr(not(feature="nightly"), proc_macro_hack)]
pub fn component(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    render(input, crate::render::RenderMode::Component)
}

fn render(input: proc_macro::TokenStream, mode: render::RenderMode) -> proc_macro::TokenStream {
    use crate::render::MacroInput;

    let MacroInput {
        options,
        mut node_list,
    } = parse_macro_input!(input as MacroInput);

    options.validate();
    node_list.inspect_no_update();

    let mut params = crate::render::Params::new(&mode, &options);
    let prepare_sup_apps_collection = match mode {
        render::RenderMode::Application => {
            params.set_sup_apps_collection_name(params.get_item_name("_sup_apps_collection_name_"));
            let sup_apps_collection_name = params.sup_apps_collection_name();
            let context = &params.context;
            quote! {
                let #sup_apps_collection_name = #context.take_sup_apps_collection();
            }
        }
        render::RenderMode::Component => proc_macro2::TokenStream::new(),
    };

    let context = &params.context;
    let root_list = params.get_item_name("root_list");
    let real_parent = quote! { #context.real_root };
    let next_sibling = quote! { #context.next_sibling };
    let first_render = params.get_item_name("first_render");
    let node_count = node_list.nodes.len();

    let generated_code = {
        let generated_render = node_list.generate(
            &params,
            &root_list,
            &real_parent,
            &next_sibling,
            &first_render,
        );
        quote::quote! {{
            let #root_list = #context.take_root_list();
            #prepare_sup_apps_collection
            let #first_render = #root_list.len() == 0;
            if #first_render {
                #root_list.new_capacity(#node_count);
            }
            #generated_render
            debug_assert_eq!(#node_count, #root_list.len(), "Simi internal bug: root_list item count mismatch");
        }}
    };

    if options.debug {
        println!("{}", generated_code);
    }
    generated_code.into()
}
