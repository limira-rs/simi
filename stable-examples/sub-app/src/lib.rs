use simi::interop::events::Event;
use simi::prelude::*;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

mod count_down;

#[simi_app]
pub struct MainApp {
    interval: usize,
    time_up: usize,
}

pub enum Msg {
    TimeUp,
    IntervalChange(Event),
    SetInterval,
}

impl Application for MainApp {
    type Message = Msg;
    type Context = ();
    type Parent = ();
    fn init() -> Self {
        wasm_logger::init(wasm_logger::Config::new(log::Level::Debug));
        Self {
            time_up: 0,
            interval: 4,
        }
    }
    fn update(&mut self, m: Self::Message, cp: ContextPlus<Self>) -> UpdateView {
        match m {
            Msg::TimeUp => self.time_up += 1,
            Msg::IntervalChange(event) => self.update_interval(event),
            Msg::SetInterval => cp
                .sub_apps
                .get::<self::count_down::CountDown>(1)
                .expect("CountDown sub app")
                .send_message(self::count_down::Msg::SetInterval(self.interval)),
        }
        true
    }
    fn render(&self, context: RenderContext<Self>) {
        simi::application! {
            @context=context
            h1 { "CountDown" }
            p { "inside a " b {"sub app"} }
            div {
                @sub_app(1, crate::count_down::CountDown)
            }
            p { "Here is the main app again. The main app only updates on `Interval` change or on TimeUp" }
            div {
                "Interval"
                input (type="number" value=self.interval onchange=Msg::IntervalChange(?))
                button (onclick=Msg::SetInterval) { "Set interval to " self.interval }
            }
            p { "Time up count: " self.time_up }

        }
    }
}

impl MainApp {
    fn update_interval(&mut self, event: Event) {
        let target = event.target().expect("Target");
        let input: &web_sys::HtmlInputElement = target.unchecked_ref();
        self.interval = input.value().parse().expect("A number");
    }
}
