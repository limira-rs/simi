use simi::prelude::*;
use wasm_bindgen::closure::Closure;
use wasm_bindgen::JsCast;

use crate::{MainApp, Msg as MainMsg};

pub struct CountDown {
    interval: usize,
    second: usize,
}

pub enum Msg {
    Tick,
    SetInterval(usize),
}

pub struct Context {
    parent: WeakMain<MainApp>,
    _ticker: Closure<Fn()>,
}

impl AppContext<CountDown> for Context {
    fn init(main: WeakMain<CountDown>, parent: WeakMain<MainApp>) -> Self {
        let _ticker = simi::interop::callback::no_arg(main, || Msg::Tick);
        web_sys::window()
            .expect("A window from browser")
            .set_interval_with_callback_and_timeout_and_arguments_0(
                _ticker.as_ref().unchecked_ref(),
                1000,
            )
            .expect("Set interval");
        Context { _ticker, parent }
    }
}

impl Application for CountDown {
    type Message = Msg;
    type Context = Context;
    type Parent = WeakMain<MainApp>;
    fn init() -> Self {
        Self {
            second: 4,
            interval: 4,
        }
    }
    fn update(&mut self, m: Self::Message, cp: ContextPlus<Self>) -> UpdateView {
        match m {
            Msg::Tick => {
                if self.second == 0 {
                    self.second = self.interval;
                    cp.context.parent.send_message(MainMsg::TimeUp);
                } else {
                    self.second -= 1;
                }
            }
            Msg::SetInterval(value) => self.interval = value,
        }
        true
    }
    fn render(&self, context: RenderContext<Self>) {
        simi::application! {
            @context=context
            fieldset {
                legend {"sub-app: " b { "CountDown" } }
                p { "Next count down will start at " self.interval }
                p { "Count down value: " self.second }
                p { "This sub app updates every second" }
            }
        }
    }
}
