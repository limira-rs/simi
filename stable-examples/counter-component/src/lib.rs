use simi::prelude::*;
use wasm_bindgen::prelude::*;

// Define a component that will have 2 event handlers
struct Counter<'a, A: Application> {
    title: &'static str,
    value: &'a i32,
    // To receive element event handlers from the parent.
    // They are Cell<Option<Box<EventHandler>>> to allow the inner handler to be taken out.
    up: ElementEventHandler<A>,
    down: ElementEventHandler<A>,
}

impl<A: Application> Component<A> for Counter<'_, A> {
    fn render(&self, context: RenderContext<A>) {
        // Here will use `component!` (not `application!`)
        simi::component! {
            //@debug
            @context=context
            p { #self.title self.value }
            //  ^ `#` tell the macro to exclude the subsequent value from updating (only init it in the first render).
            //        because we know that the title will never change

            button (onclick=#self.down) { "Down" }
            //               ^^^^^ the macro assumes that an event handler start with `self.` is a placeholder for
            //                     the actual handler that will be passed here later
            //              ^ `#`: the event handler will also be excluded from updating. If a new handler was passed here
            //                     on update, it will be ignored.

            button (onclick=#self.up) { "Up" }
        }
    }
}

#[simi_app]
struct Counters {
    month: i32,
    year: i32,
}

enum Msg {
    MonthUp,
    MonthDown,
    YearUp,
    YearDown,
}

impl Application for Counters {
    type Message = Msg;
    type Context = ();
    type Parent = ();
    fn init() -> Self {
        Self {
            month: 12,
            year: 2018,
        }
    }
    fn update(&mut self, m: Self::Message, _: ContextPlus<Self>) -> UpdateView {
        match m {
            Msg::MonthUp => {
                self.month += 1;
                if self.month > 12 {
                    self.year += 1;
                    self.month = 1;
                }
            }
            Msg::MonthDown => {
                self.month -= 1;
                if self.month < 1 {
                    self.year -= 1;
                    self.month = 12;
                }
            }
            Msg::YearUp => self.year += 1,
            Msg::YearDown => self.year -= 1,
        }
        true
    }
    fn render(&self, context: RenderContext<Self>) {
        simi::application! {
            //@debug
            @context=context
            h1 { "Counters" }
            p { "using a " b {"simple component"} }
            hr

            // A CamelCase identifier will be intepreted as a simi's component
            Counter {
                title: "Month: ",
                value: &self.month,

                // This event handler is never change, but the macro `application!` do not understand it,
                // therefore, if you want to exclude it from updating, prefix it with `#`, then the macro will
                // pass `None` to the component on updating. Otherwise, a new event handler will be created
                // and pass to the component everytime `render` execute.
                up: onclick=#Msg::MonthUp,
                //  ^^^^^^^^ we need these tokens for an element event, because the macro doesn't know
                // anything about the component, hence, it doesn't known what kind of event to create here
                // unless we tell it that the event is `onclick`

                down: onclick=#Msg::MonthDown,
            }

            // A second use of component Counter
            Counter {
                title: "Year: ",
                value: &self.year,
                up: onclick=#Msg::YearUp,
                down: onclick=#Msg::YearDown,
            }
        }
    }
}
