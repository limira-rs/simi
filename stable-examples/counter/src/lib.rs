use simi::prelude::*;
use wasm_bindgen::prelude::*;

#[simi_app]
struct Counter {
    count: i32,
}

enum Msg {
    Up,
    Down,
}

impl Application for Counter {
    type Message = Msg;
    type Context = ();
    type Parent = ();
    fn init() -> Self {
        Self { count: 2018 }
    }
    fn update(&mut self, m: Self::Message, _: ContextPlus<Self>) -> UpdateView {
        match m {
            Msg::Up => self.count += 1,
            Msg::Down => self.count -= 1,
        }
        true
    }
    fn render(&self, context: RenderContext<Self>) {
        simi::application! {
            //@debug
            @context=context
            h1 { "Counter" }
            p { "the " b {"simplest"} " version" }
            hr
            button (onclick=#Msg::Down) { "Down" }
            span { " " self.count " " }
            button (onclick=#Msg::Up) { "Up" }
        }
    }
}
