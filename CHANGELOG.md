#Simi Change Log

## v0.2.2
Released on [not yet]

### Added
* Add support for window's events

### Changed
* [Breaking] mod `simi::element_events` renamed to `simi::events`
* [Breaking] mod `simi::interop::element_events` renamed to `simi::interop::events`

## v0.2.1
Released on 2019-05-04

### Added
* Preleminary support for stable Rust.

## v0.2.0
Released on 2018-12-31

Features:
* if/match
* Non key for
* Component
* sub_app, tab_apps
* fetch
